// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig:{
    apiKey: "AIzaSyAcbJ85_Yi2Hzk41qJeV8-qZOtr8fz95sU",
    authDomain: "holaferia-mjccnop.firebaseapp.com",
    databaseURL: "https://holaferia-mjccnop.firebaseio.com",
    projectId: "holaferia-mjccnop",
    storageBucket: "holaferia-mjccnop.appspot.com",
    messagingSenderId: "882091509670"
  }
};
