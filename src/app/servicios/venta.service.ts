// HACERLO INJECTABLE PARA TOMAR OTROS SERVICIOS
import { Injectable } from '@angular/core';
// IMPORTAR SERVICIO DE BE
import { VentaBackService } from '../firebase/venta-back.service';
// IMPORTAR SERVICIOS DE UI
import { MedioPagoService } from './medioPago.service';
import { JornadaService } from './jornada.service';
import { StockService } from './stock.service';
import { AuthService } from './auth.service';
// IMPORTAR ENTIDADES
import { Venta } from '../entidades/venta';
import { Articulo } from '../entidades/articulo';
// IMPORTAR UTILS
import Utils from '../configuraciones/utils';
import { EstadisticaService } from './estadistica.service';


@Injectable()
export class VentaService {
  ventaEnCurso: Venta = new Venta();

  constructor(
    private _medioPagoService: MedioPagoService,
    private _jornadaService: JornadaService,
    private _estadisticaService: EstadisticaService,
    private _stockService: StockService,
    private _backService: VentaBackService,
    private _authService: AuthService
  ) {}

  buscarArticulo(_art: Articulo, i: number) {
    if (this.ventaEnCurso.pagos.length === 0) {
      this.ventaEnCurso.pagos = this._medioPagoService.mediosPago;
    }

    const _articulo =
      this._stockService.listaArticulos.find(
        _artStock => {
          return Utils.compararTexto(_artStock.codigo.valor, _art.codigo.valor);
      });

    const _noDuplicados =
      _articulo &&
      !(
        this.ventaEnCurso.articulos.find(
          _artStock => {
            return Utils.compararTexto(_artStock.codigo.valor, _articulo.codigo.valor)
             && Utils.compararTexto(_artStock.descripcion.valor, _articulo.descripcion.valor);
          })
      );

    if (_noDuplicados) {
      this.ventaEnCurso.articulos[i] = this._jornadaService.setearDescuento(_articulo);
    }
  }

  sumarPrecios() {
    this.filtrarArticulosNulos();
    this.ventaEnCurso.sumarTotal();
  }

  agregarArticuloVenta() {
    this.ventaEnCurso.agregarArticulo();
  }

  quitarArticulo(i: any) {
    this.ventaEnCurso.removerArticulo(i);
  }

  filtrarArticulosNulos() {
    this.ventaEnCurso.articulos.forEach(
      (_art, i) => {
        if (!_art.codigo.valor || !_art.codigo.esValido) {
          this.ventaEnCurso.articulos.splice(i, 1)
        }
      }
    )
  }

  finalizarVenta() {
    const _venta = this.ventaEnCurso;

    _venta.filtarArticulosNulos();

    _venta.articulos.forEach(
      art => this._stockService.eliminarArticulo(art, false)
    );

    _venta.pagos.forEach(
      _pago => {
        if (_pago.pagoParcial.valor === '') _pago.pagoParcial.valor = '0';
      }
    );

    _venta.vendedor = this._authService.usuarioAuth.nombre.valor;

    this._backService.crear_venta(_venta);
    this._jornadaService.actualizarJornada(_venta.totalEfectivo, _venta.totalTarjetas);
    this._estadisticaService.actualizarEstadisticas(_venta.totalEfectivo, _venta.totalTarjetas)
    _venta.limpiarVenta();
  }

  get ventaValida(): boolean {
    return this.ventaEnCurso.esValida;
  }

  get sonArticulosValidos(): boolean {
    return this.ventaEnCurso.sonArticulosValidos;
  }
}
