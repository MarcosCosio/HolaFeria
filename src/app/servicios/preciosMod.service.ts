import { Injectable } from '@angular/core';
import { PrecioMod } from '../entidades/precioMod';
import { PreciosModBackService } from '../firebase/precios-mod-back.service';

@Injectable({
  providedIn: 'root'
})
export class PreciosModService {
  listaPreciosMod: PrecioMod[] = [];
  precioMod: PrecioMod = new PrecioMod();

  constructor(
    private _backService: PreciosModBackService
  ) {
    this._backService.buscar_ventas_borradas().subscribe(
      _lista => {
        this.listaPreciosMod = _lista.map(
          (_item, i) => {
            return new PrecioMod({
              $key: _item.key,
              id: i,
              ..._item.payload.val()
            })
          }).reverse();
      }
    )
  }

  agregarPrecioMod() {
    this._backService.registrar_stock_viejo(this.precioMod);
  }
}
