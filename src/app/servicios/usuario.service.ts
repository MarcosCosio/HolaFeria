import { Injectable } from '../../../node_modules/@angular/core';
// IMPORTAR SERVICIO DE BE
import { UserBackService } from '../firebase/user-back.service';
// IMPORTAR ENTIDADES
import { Usuario } from '../entidades/usuario';
// IMPORTAR MOCKS
import Mocks from '../entidades/mocks/usuarios.mock';
// IMPORTAR UTILIDADES
import Utils from '../configuraciones/utils';

@Injectable()
export class UserService {
  listaUsuarios: Usuario[] = [];
  usuarioMod: Usuario = new Usuario();

  constructor(
    private _backService: UserBackService
  ) {
    if (Utils.habilitarMocks) {
      this.listaUsuarios.push(new Usuario(Mocks));
    } else {
      this._backService.buscar_bd().subscribe(
        _lista => {
          this.listaUsuarios = _lista.map(
            (item, i) => {
              return new Usuario({
                $key: item.key,
                id: i,
                ...item.payload.val()
              });
          });
        }
      );
    }
  }

  cargarBd() {
    Mocks.forEach(
      item => {
        item.creadoEl = new Date().toISOString();
      }
    );

    this._backService.crear_bd(Mocks);
  }

  crearUsuario() {
    this.usuarioMod.creadoEl.valorPrevio = new Date().toISOString();
    this._backService.insertar_item_bd(this.usuarioMod);
    this.limpiarUsuario();
  }

  verDatosUsuario(i: any) {
    this.usuarioMod = this.listaUsuarios[i];
  }

  cambiarEstadoMod() {
    this.usuarioMod.cambiarEdicion();
  }

  modificarUsuario() {
    const _user = this.usuarioMod;

    if (_user.estado.valorPrevio && !_user.estado.valor) {
      _user.bloqueadoEl.valorPrevio = new Date().toISOString();
    }

    _user.pass.valor = _user.passNueva.valor ?
      _user.passNueva.valor : _user.pass.valorPrevio;

    this._backService.modificar_item_bd(_user);
    this.limpiarUsuario();
  }

  limpiarUsuario() {
    this.usuarioMod = new Usuario();
  }

  get usuarioEsValido(): boolean {
    return this.usuarioMod.esValido && !this.compararPassConListado(this.usuarioMod.pass.valor);
  }

  get usuarioModEsValido(): boolean {
    return this.usuarioMod.esValidoMod && !this.compararPassConListado(this.usuarioMod.passNueva.valor);
  }

  private compararPassConListado(nuevaPass: string) {
    return nuevaPass &&
      this.listaUsuarios.find(
        usuario => usuario.pass.valor === nuevaPass
      );
  }
}
