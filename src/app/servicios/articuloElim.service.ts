import { Injectable } from '@angular/core';
// IMPORTAR ENTIDADES
import { ArticuloEliminado } from '../entidades/articuloElim';
import { ArticuloElimBackService } from '../firebase/articulo-elim-back.service';

@Injectable()
export class ArticuloElimService {
  listaArticulosElim: ArticuloEliminado[];

  constructor(
    private _backService: ArticuloElimBackService
  ) {
    this._backService.buscar_articulos_elim().subscribe(
      lista => {
        this.listaArticulosElim = lista.map(
          item => {
            return new ArticuloEliminado({
              ...item.payload.val()
            });
          }
        );
      }
    )
  }

  agregarArticulo(articulo: ArticuloEliminado) {
    this._backService.insertar_articulo_elim(articulo);
  }
}
