import { Injectable } from '@angular/core';
// IMPORTAR SERVICIOS DE BE
import { MedioPagoBackService } from '../firebase/medio-pago-back.service';
// IMPORTAR ENTIDADES
import { MedioPago } from '../entidades/medioPago';
// IMPORTAR MOCKS
import MocksMediosPago from '../entidades/mocks/medioPago.mock';
// IMPORTAR UTILIDADES
import Utils from '../configuraciones/utils';

@Injectable()
export class MedioPagoService {
  mediosPago: MedioPago[] = [];

  constructor(
    private _backService: MedioPagoBackService
  ) {
    if (Utils.habilitarMocks) {
      MocksMediosPago.map(
        _medio =>
          this.mediosPago.push(new MedioPago(_medio))
        );
    } else {
      this._backService.buscar_medioPago().subscribe(
        list => {
          list.map(
            item => {
              const _list = item.payload.val();
              this.mediosPago = _list.map(
                _item => {
                  _item.$key = item.key
                  return new MedioPago(_item)
                }
              )
            }
          )
        }
      )
    }
  }

  cargarBd() {
    this._backService.init_medioPago();
  }

  eliminarBd() {
    this._backService.eliminar_Bd();
  }
}
