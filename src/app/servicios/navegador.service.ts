import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';
// IMPORTAR ENTIDAD DE MODULOS
import { ModulosConfig } from '../entidades/modulos';

@Injectable()
export class NavegadorService {
  modulos: ModulosConfig = new ModulosConfig();

  constructor(
    private router: Router
  ) { }

  procesarRuta(ruta: any) {
    const _modulo = typeof ruta === 'string' ? ruta : ruta[0],
          _estaEnRaiz = typeof ruta === 'string';

    if (_estaEnRaiz) {
      ruta = (!ruta || ruta === '') ? '' : ruta;

      this.router.navigate([ruta]).then(
        (result) => {
          this.setearRuta(result, ruta, _modulo);
        }
      );
    } else {
      this.router.navigate(ruta).then(
        (result) => {
          this.setearRuta(result, ruta, _modulo);
        }
      );
    }
  }

  private setearRuta(result: any, ruta: any, nombreModulo: any) {
    if (result !== false) {
      const _modulo = nombreModulo === '' ? '#ventas' : `#${nombreModulo}`
      this.modulos.setearRuta(ruta);

      $('.btn').removeClass('active');
      $(_modulo).addClass('active');
    }
  }
}
