import { Injectable } from '@angular/core';
// IMPORTAR SERVICIO DE BE
import { TipoArticuloBackService } from '../firebase/tipo-articulo-back.service';
// IMPORTAR ENTIDADES
import { TipoArticulo } from '../entidades/tipoArticulo';
// IMPORTAR MOCKS
import MocksTipoArticulos from '../entidades/mocks/tipoArticulos.mock';
// IMPORTAR UTILIDADES
import Utils from '../configuraciones/utils';

@Injectable()
export class TipoArticuloService {
  articulosEnStock: TipoArticulo[] = [];
  copiaArticulos: any;

  constructor(
    private _backService: TipoArticuloBackService
  ) {
    if (Utils.habilitarMocks) {
      MocksTipoArticulos.forEach(_tipo => {
        this.articulosEnStock.push(new TipoArticulo(_tipo));
        this.articulosEnStock.forEach(_tArt => {
          _tArt.ordenar();
        });
      });
    } else {
      this._backService.buscar_tipoArticulo().subscribe(
        list => {
          list.map(
            item => {
              const _lista = item.payload.val();

              this.articulosEnStock =
                _lista.map(
                  _item => {
                    _item.$key = item.key
                    return new TipoArticulo(_item);
                  }
                );

              this.copiaArticulos = JSON.stringify(this.articulosEnStock);
          });
      });
    }
  }

  eliminarBd() {
    this._backService.eliminar_Bd();
  }

  cargarBd() {
    this._backService.init_tipoArticulo();
  }

  armarCodigo(codigoOriginal: any, armarCodigoCompleto: boolean) {
    const _tipoFiltrado =
      this.articulosEnStock.filter(item => {
        return item.codigo === codigoOriginal.toUpperCase();
      })[0];
    let _codigoArmado = null;

    if (_tipoFiltrado) {
      if (armarCodigoCompleto) {
        const _cantidadArt = _tipoFiltrado.articulos.length;
        let _nuevaPosicion = 1;

        switch (_cantidadArt) {
          case 0 : {
            _tipoFiltrado.articulos.push(_nuevaPosicion);
            _codigoArmado = this.nuevoCodigo(_tipoFiltrado, _nuevaPosicion);
            break;
          }
          case 1: {
            if (_nuevaPosicion === _tipoFiltrado.articulos[0]) {
              _nuevaPosicion++;
            }

            _tipoFiltrado.articulos.push(_nuevaPosicion);
            _codigoArmado = this.nuevoCodigo(_tipoFiltrado, _nuevaPosicion);
            break;
          }
          default: {
            _tipoFiltrado.ordenar();

            while(this.elCodigoEsIgual(_tipoFiltrado.articulos, _nuevaPosicion)) {
              _nuevaPosicion++;
            }

            _tipoFiltrado.articulos.push(_nuevaPosicion);
            _codigoArmado = this.nuevoCodigo(_tipoFiltrado, _nuevaPosicion);
            break;
          }
        }
      } else {
        _codigoArmado = _tipoFiltrado.codigo.toUpperCase();
      }
    }

    return _codigoArmado || codigoOriginal;
  }

  private elCodigoEsIgual(articulosCargados: number[], nuevaPosicion: number): boolean {
    return articulosCargados.filter(
        _posicion => {
          return _posicion === nuevaPosicion;
        }
      ).length > 0;
  }

  actualizarBd() {
    const key = this.articulosEnStock[0].$key;
    const _nuevosArticulos = this.articulosEnStock.map(
        tipoArt => tipoArt.toJS()
      );
    this._backService.editar_tipoArticulo(key, _nuevosArticulos);
  }

  eliminarTipoArt(codigo: string) {
    const _textoCodigo = this.buscarTextoCodigo(codigo);
    const _numeroCodigo = _textoCodigo ? +(codigo.split(_textoCodigo)[1]) : 0;
    const _articuloEncontrado =
      this.articulosEnStock.find(
          _tipo => _tipo.codigo === _textoCodigo
        );

    if (_articuloEncontrado) {
      const _lista = _articuloEncontrado.articulos;
      const _index = _lista.indexOf(_numeroCodigo)

      _lista.splice(_index, 1);
    }
  }

  traerListadoTipos() {
    return this.articulosEnStock.map(
      _art => { return `${_art.codigo}: ${_art.descripcion}`; }
    );
  }

  traerCantidadesTipos() {
    let _totalArt: number = 0;
    let _listaTotales =
      this.articulosEnStock.filter(
        _art => {
          return _art.articulos && _art.articulos.length > 0;
        }
      ).map(
        _art => {
          _totalArt += _art.articulos.length;
          return `${_art.descripcion}: ${_art.articulos.length}`;
        }
      )

    _listaTotales.unshift(`Articulos cargados: ${_totalArt}`);
    return _listaTotales;
  }

  renovarListado() {
    this.copiaArticulos = JSON.parse(this.copiaArticulos);

    this.articulosEnStock =
      this.copiaArticulos.map(
        _item => {
          return new TipoArticulo(_item);
        }
      );

    this.copiaArticulos = JSON.stringify(this.articulosEnStock);
  }

  private nuevoCodigo(_listado: any, posicion: any): string {
    return _listado.codigo.toUpperCase().concat(posicion.toString());
  }

  private buscarTextoCodigo(codigo: string): string {
    return this.articulosEnStock.find(
      _art => codigo.includes(_art.codigo)
    ).codigo
  }
}
