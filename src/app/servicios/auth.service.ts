import { Injectable } from "@angular/core";
// IMPORTAR SERVICIO DE BE
import { UserBackService } from "../firebase/user-back.service";
// IMPORTAR ENTIDADES
import { Usuario } from "../entidades/usuario";

@Injectable()
export class AuthService {
  usuarioAuth: Usuario;

  constructor(
    private _userBackService: UserBackService
  ) {}

  compararPass(pass: string, soloAdmin: boolean = false): boolean {
    let _resultado = this._userBackService.obtener_usuario(pass, soloAdmin);

    if (_resultado instanceof Usuario && _resultado.estado.valor) {
      this.usuarioAuth = _resultado;
      this.usuarioAuth.ultimoAcceso.valorPrevio = new Date().toISOString();

      this._userBackService.modificar_item_bd(this.usuarioAuth);
    } else {
      _resultado = false;
    }

    return Boolean(_resultado);
  }
}
