import { Injectable } from '@angular/core';
// IMPORTAR SERVICIOS DE UI
import { NavegadorService } from './navegador.service';
import { JornadaService } from './jornada.service';
import { AuthService } from './auth.service';
// IMPORTAR SERVICIOS DE BE
import { EstadisticaBackService } from '../firebase/estadistica-back.service';
// IMPORTAR ENTIDADES
import { Estadistica } from '../entidades/estadistica';
import { Jornada } from '../entidades/jornada';
import { Venta } from '../entidades/venta';
import { Articulo } from '../entidades/articulo';
// IMPORTAR MOMENT PARA COMPARAR FECHAS
import * as moment from 'moment';

@Injectable()
export class EstadisticaService {
  ultimasJornadas: Jornada[] = [];
  listaVentas: Venta[] = [];
  listaJornadas: Jornada[] = [];
  listaMeses: Estadistica[] = [];
  listaArticulosVendidos: Articulo[] = [];
  detallesVenta: Venta;
  detallesJornada: Jornada;
  detallesEstad: Estadistica;
  estadisticaActual: Estadistica;

  constructor(
    private _navService: NavegadorService,
    private _jornadaService: JornadaService,
    private _backService: EstadisticaBackService
  ) {
    this._backService.buscar_estadisticas().subscribe(
      _lista => {
        this.listaMeses =
          _lista.map(
            (item) => {
              return new Estadistica({
                $key: item.key,
                ...item.payload.val()
              });
            }
          ).reverse().map(
            (_item, _i) => {
              _item.id = _i;
              return _item;
            }
          );

        this.estadisticaActual = this.listaMeses[0];
      }
    );

    this.ultimasJornadas = this._jornadaService.ultimasJornadas;

    this._backService.obtener_lista_articulos_vendidos()
      .then(
        _resultado => this.listaArticulosVendidos = _resultado
      ).catch(
        error => console.error(error)
      )
  }

  traerUltimaEstadistica() {
    this._backService.obtener_ultima_estadistica()
      .then(
        (_estadisticas: Estadistica) => {
          this.estadisticaActual = _estadisticas;
        }
      )
      .catch(
        (_error) => console.error(_error)
      )
  }

  crearNuevaEstadistica() {
    const _nuevaEstad = new Estadistica(null);
    _nuevaEstad.fecha.valor = new Date().toISOString();

    this._backService.crear_estadistica(_nuevaEstad);
  }

  iniciarJornada() {
    this._jornadaService.iniciarJornada();
    this._jornadaService.traerUtimaJornada()
      .then(
        (_jornadaBack: Jornada) => {
          const _ultimaJ = _jornadaBack.fechaInicio.valor;
          const _ultimaE = this.listaMeses[0].fecha.valor;
          const _mismoMes = moment(_ultimaJ).isSame(_ultimaE, 'month');
          const _mismoAnio = moment(_ultimaJ).isSame(_ultimaE, 'year');

          if(!(_mismoAnio && _mismoMes)) {
            this.crearNuevaEstadistica();
          }

          this.traerUltimasJornadas();
        }
      )
      .catch(
        () => { return; }
      );
  }

  actualizarEstadisticas(ventaEfectivo: number, ventaTarjeta: number) {
    this.estadisticaActual.montoEfectivo += +ventaEfectivo;
    this.estadisticaActual.montoTarjeta += +ventaTarjeta;

    this._backService.mod_estadistica(this.estadisticaActual);
  }

  traerUltimasJornadas() {
    this._backService.obtener_ultimas_jornadas()
      .then(
        (_jornadaBack: Jornada[]) => {
          this.ultimasJornadas =
            _jornadaBack.reverse().map(
              (_item, i) => {
                _item.id = i
                return _item
              }
            );
        }
      )
      .catch(
        (_error) => console.warn(_error)
      )
  }

  verDetallesMes(i: any = null, esLlamadaInterna: boolean = false) {
    if (i !== null) {
      this.detallesEstad = this.listaMeses[i];
    } else {
      i = this.detallesEstad.id;
    }

    this._backService.obtener_lista_jornadas(this.detallesEstad.fechaCorta)
      .then(
        (_res: Jornada[]) => {
          this.listaJornadas = _res;

          if (!esLlamadaInterna) {
            this._navService.procesarRuta(['estadisticas','ver',i])
          }
        }
      )
      .catch(
        () => {
          this.listaJornadas = [];
        }
      );
  }

  verDetallesJornada(i: any = null, desdeUltimasJornadas: boolean = false) {
    if (i !== null) {
      if (desdeUltimasJornadas) {
        this.detallesEstad = this.listaMeses[0];
        this.verDetallesMes(null, true);
      }

      this.detallesJornada = desdeUltimasJornadas ?
        this.ultimasJornadas[i] : this.listaJornadas[i];
    } else {
      i = this.detallesJornada.id;
    }

    this._backService.obtener_lista_ventas(this.detallesJornada.fechaCorta)
      .then(
        (_res: Venta[]) => {
          this.listaVentas =
            _res.filter(
              (_venta) => {
                return this.esEntreFechas(_venta.fechaVenta.valor,
                  this.detallesJornada.fechaInicio.valor,
                  this.detallesJornada.fechaFin.valor)
              }
            )

          if (this.listaVentas.length > 0) {
            this.listaVentas =
              this.listaVentas.reverse().map(
                (_item, _i) => {
                  _item.id = _i;
                  return _item;
                }
              );
          }
          this._navService.procesarRuta(['estadisticas','modificar',i]);
        }
      )
      .catch(
        () => {
          this.listaVentas = [];
          this._navService.procesarRuta(['estadisticas','modificar',i]);
        }
      )
  }

  verDetallesVenta(i: any = null) {
    if (i !== null) {
      this.detallesVenta = this.listaVentas[i];
    } else {
      i = this.detallesVenta.id;
    }

    this._navService.procesarRuta(['estadisticas','crear',i]);
  }

  verArticulosVendidos() {
    this._backService.obtener_lista_articulos_vendidos()
      .then(
        _resultado => this.listaArticulosVendidos = _resultado
      )
      .catch(
        error => console.error(error)
      )
  }

  buscarArticulosVend(busqueda: string) {
    const _hayConsulta: boolean =
      busqueda !== undefined &&
      busqueda !== null &&
      busqueda.length > 0;

    busqueda = _hayConsulta ? busqueda.toUpperCase() : '';

    this.listaArticulosVendidos.forEach(
      _art => {
        _art.mostrar = _hayConsulta ?
          (
            _art.codigo.valor.toUpperCase().includes(busqueda) ||
            _art.descripcion.valor.toUpperCase().includes(busqueda)
          ) : true;
    });
  }

  private esEntreFechas(fechaVenta: string, fechaJorInicio: string, fechaJorFinal: string = null): boolean {
    return fechaJorFinal ?
        moment(fechaVenta).isBetween(fechaJorInicio, fechaJorFinal) :
        moment(fechaVenta).isSameOrAfter(fechaJorInicio);
  }
}
