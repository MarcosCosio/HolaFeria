import { Injectable } from '@angular/core';
// IMPORTAR SERVICIOS DE UI
import { TipoArticuloService } from './tiposArt.service';
import { PreciosModService } from './preciosMod.service';
import { AuthService } from './auth.service';
// IMPORTAR SERVICIO DE BE
import { ArticuloBackService } from '../firebase/articulo-back.service';
// IMPORTAR ENTIDADES
import { Articulo } from '../entidades/articulo';
import { PrecioMod } from '../entidades/precioMod';
// IMPORTAR MOCKS
import MocksArticulos from '../entidades/mocks/articulos.mock';
// IMPORTAR UTILIDADES
import Utils from '../configuraciones/utils';
import { ArticuloElimService } from './articuloElim.service';
import { ArticuloEliminado } from '../entidades/articuloElim';


@Injectable()
export class StockService {
  listaArticulos: Articulo[] = [];
  stockPorCargar: Articulo[] = [new Articulo(null)];
  listaTotalesPorArticulo: string[];
  precioMod: PrecioMod;
  listaPreciosMod: PrecioMod[];

  constructor(
    private _tipoArtService: TipoArticuloService,
    private _precioModService: PreciosModService,
    private _articuloElimService: ArticuloElimService,
    private _authService: AuthService,
    private _backService: ArticuloBackService,
  ) {
    if (Utils.habilitarMocks) {
      MocksArticulos.forEach(_art => {
        this.listaArticulos.push(new Articulo(_art));
      });
    } else {
      this._backService.buscar_articulos().subscribe(
        list => {
          this.listaArticulos =
            list.map(
              item => {
                return new Articulo({
                  $key: item.key,
                  ...item.payload.val()
                });
            }).sort(
              (_artA, _artB) => {
                const _textoA = _artA.codigo.soloTexto,
                      _textoB = _artB.codigo.soloTexto,
                      _numeroA = _artA.codigo.soloNumero,
                      _numeroB = _artB.codigo.soloNumero;

                return (_textoA.localeCompare(_textoB) < 0) ||
                (_textoA.localeCompare(_textoB) === 0 && _numeroA < _numeroB) ?
                -1 : 1;
              }
            );

          this.listaArticulos.forEach(
            (_item, _i) => _item.id = _i
          );

          this.listaTotalesPorArticulo = this._tipoArtService.traerCantidadesTipos();
      });

      this.precioMod = this._precioModService.precioMod;
      this.listaPreciosMod = this._precioModService.listaPreciosMod;
    }
  }

  get hayArticulos(): boolean {
    return this.listaArticulos.length > 0 &&
      this.listaArticulos.filter(
        art => { return art.mostrar }
      ).length > 0;
  }

  get precioModificadoValido() {
    const _precioValido = this.listaArticulos.find(
      _art => {
        return _art.cambiarPrecio === true;
      }
    ).precioStock.esValido,
        _precioModValido = this.precioMod.descripcion.esValido;

    return _precioValido && _precioModValido;
  }

  get articuloModificado(): Articulo {
    return this.listaArticulos.find(
      _art => {
        return _art.cambiarPrecio === true;
      }
    );
  }

  get stockValido(): boolean {
    const _listaCant = this.stockPorCargar.length;
    const _esValido = _listaCant === 1 && this.stockPorCargar[0].esValido;
    const _sonValidos = _listaCant > 1 &&
      this.stockPorCargar.filter(
        (_art, i) => {
          return i === _listaCant - 1 ?
            _art.esValido : !_art.esValido;
        }).length === 0;

    return (_esValido || _sonValidos);
  }

  mostrarPrecioUi(art: Articulo): void {
    this.listaArticulos.forEach(
      _art => {
        _art.mostrar = _art.id === art.id;
        _art.cambiarPrecio =  _art.id === art.id;
      }
    )
  }

  modificarPrecio() {
    const _precioModObj =
      {
        fecha: new Date().toISOString(),
        articulo: `${this.articuloModificado.darCodigoNombre}`,
        precioViejo: this.articuloModificado.precioStock.valorPrevio,
        precioNuevo: this.articuloModificado.precioStock.valor,
        autorizacion: this._authService.usuarioAuth.nombre.valor,
        descripcion: this.precioMod.descripcion.valor
      };

    this._precioModService.precioMod = new PrecioMod(_precioModObj);

    this._precioModService.agregarPrecioMod();
    this._backService.mod_articulo(this.articuloModificado);
    this.cancelarCambiarPrecio();
  }

  cancelarCambiarPrecio(restaurarPrecio: boolean = false): void {
    if (restaurarPrecio) {
      this.articuloModificado.restaurarPrecio();
    } else {
      this.articuloModificado.actualizarPrecio();
    }

    this.listaArticulos.forEach(
      _art => {
        _art.mostrar = true;
        _art.cambiarPrecio =  false;
      }
    )
  }

  buscarArticulos(busqueda: string) {
    const _hayConsulta: boolean =
      busqueda !== undefined &&
      busqueda !== null &&
      busqueda.length > 0;

    busqueda = _hayConsulta ? busqueda.toUpperCase() : '';

    this.listaArticulos.forEach(
      _art => {
        _art.mostrar = _hayConsulta ?
          (
            _art.codigo.valor.toUpperCase().includes(busqueda) ||
            _art.descripcion.valor.toUpperCase().includes(busqueda)
          ) : true;
    });
  }

  sumarAlListado() {
    this.stockPorCargar.push(new Articulo(null));
  }

  cargarAlStock() {
    this.filtrarArticulosNulos();
    this._tipoArtService.actualizarBd();

    this.stockPorCargar.map(
      art => this._backService.insertar_articulo(art)
    );

    this.limpiarListado();
  }

  eliminarArticulo(art: any, soloDelStock: boolean = true) {
    if(!(art instanceof Articulo)) {
      art = this.listaArticulos.find(
        _art => _art.$key === art
      )
    }

    if (soloDelStock) {
      const _articuloElim =
        new ArticuloEliminado({
          codigo: art.codigo.valor,
          descripcion: art.descripcion.valor,
          precioVenta: art.precioVenta.valor,
          fechaEliminado: new Date().toISOString(),
          autorizacion: this._authService.usuarioAuth.nombre.valor,
        });

      this._articuloElimService.agregarArticulo(_articuloElim);
    }

    this.listaArticulos.splice(art.id, 1);

    this.listaArticulos.forEach(
      (_item, _i) => {
        _item.id = _i
      }
    );

    this._tipoArtService.eliminarTipoArt(art.codigo.valor);
    this._tipoArtService.actualizarBd();
    this._backService.eliminar_articulos(art.$key)
  }

  eliminarBd() {
    this._backService.eliminar_bd();
  }

  limpiarListado() {
    this.stockPorCargar = [new Articulo(null)];
  }

  mostrarListado() {
    this.listaArticulos.map(
      art => art.mostrar = true
    )
  }

  filtrarArticulosNulos() {
    this.stockPorCargar.forEach(
      (_art, i) => {
        const _codigoInvalido = !_art.codigo.valor || !_art.codigo.esValido,
              _codigoErroneo = this._tipoArtService.articulosEnStock.find(
                _tipoArt => {
                  return _tipoArt.codigo === _art.codigo.soloTexto
                }
              );

        if (_codigoInvalido || !_codigoErroneo) {
          this.stockPorCargar.splice(i, 1)
        }
      }
    )
  }
}
