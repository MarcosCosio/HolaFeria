import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// COMPONENTES => VENTAS
import { VentasComponent } from './modulos/ventas/ventas.component';
import { NuevaJornadaComponent } from './modulos/ventas/nueva-jornada/nueva-jornada.component';
import { NuevaVentaComponent } from './modulos/ventas/nueva-venta/nueva-venta.component';
import { TerminarVentaComponent } from './modulos/ventas/terminar-venta/terminar-venta.component';
// COMPONENTES => STOCK
import { StockComponent } from './modulos/stock/stock.component';
import { ListarStockComponent } from './modulos/stock/listar-stock/listar-stock.component';
import { AgregarStockComponent } from './modulos/stock/agregar-stock/agregar-stock.component';
import { VerPreciosModComponent } from './modulos/stock/ver-precios-mod/ver-precios-mod.component';
import { VerArticulosDelComponent } from './modulos/stock/ver-articulos-del/ver-articulos-del.component';
// COMPONENTES => ESTADISTICAS
import { EstadisticasComponent } from './modulos/estadisticas/estadisticas.component';
import { ListarEstadisticasComponent } from './modulos/estadisticas/listar-estadisticas/listar-estadisticas.component';
import { VerEstadisticaComponent } from './modulos/estadisticas/ver-estadistica/ver-estadistica.component';
import { VerJornadaComponent } from './modulos/estadisticas/ver-jornada/ver-jornada.component';
import { VerVentaComponent } from './modulos/estadisticas/ver-venta/ver-venta.component';
import { VerArticulosVendComponent } from './modulos/estadisticas/ver-articulos-vend/ver-articulos-vend.component';
// COMPONENTES => USUARIOS
import { UsuariosComponent } from './modulos/usuarios/usuarios.component';
import { ListarUsuariosComponent } from './modulos/usuarios/listar-usuarios/listar-usuarios.component';
import { CrearUsuarioComponent } from './modulos/usuarios/crear-usuario/crear-usuario.component';
import { ModificarUsuarioComponent } from './modulos/usuarios/modificar-usuario/modificar-usuario.component';
// GUARDAS
import { SeguridadGuard } from './guardas/seguridad.guard';

// RUTAS
const appRoutes: Routes = [
  { path: '', redirectTo: '/ventas', pathMatch: 'full' },
  {
    path: 'ventas',
    component: VentasComponent,
    children: [
      { path: '', component: VentasComponent },
      { path: 'ver', component: NuevaJornadaComponent },
      { path: 'crear', component: NuevaVentaComponent },
      { path: 'modificar', component: TerminarVentaComponent }
    ]
  },
  {
    path: 'stock',
    component: StockComponent,
    canActivate: [SeguridadGuard],
    children: [
      { path: '', component: ListarStockComponent },
      { path: 'crear', component: AgregarStockComponent },
      { path: 'modificar', component: VerPreciosModComponent },
      { path: 'eliminar', component: VerArticulosDelComponent}
    ]
  },
  {
    path: 'estadisticas',
    component: EstadisticasComponent,
    canActivate: [SeguridadGuard],
    children: [
      { path: '', component: ListarEstadisticasComponent },
      { path: 'ver/:id', component: VerEstadisticaComponent },
      { path: 'modificar/:id', component: VerJornadaComponent },
      { path: 'crear/:id', component: VerVentaComponent },
      { path: 'eliminar', component: VerArticulosVendComponent }
    ]
  },
  {
    path: 'usuarios',
    component: UsuariosComponent,
    canActivate: [SeguridadGuard],
    children: [
      { path: '', component: ListarUsuariosComponent },
      { path: 'crear', component: CrearUsuarioComponent },
      { path: 'ver/:id', component: ModificarUsuarioComponent },
      { path: 'modificar/:id', component: ModificarUsuarioComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class RoutingModule {}
