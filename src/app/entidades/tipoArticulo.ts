export class TipoArticulo {
  codigo: string;
  descripcion: string;
  articulos: number[];
  // VARIABLES NO ENVIADAS A BE
  $key: string;

  constructor(_obj: any) {
    this.codigo = _obj.codigo;
    this.descripcion = _obj.descripcion;
    this.articulos = _obj.articulos ? _obj.articulos : [];
    // VARIABLES NO ENVIADAS A BE
    this.$key = _obj ? _obj.$key : null;
  }

  ordenar() {
    this.articulos.sort((a, b) => a - b);
  }

  toJS() {
    return {
      'codigo': this.codigo,
      'descripcion': this.descripcion,
      'articulos': this.articulos
    }
  }
}
