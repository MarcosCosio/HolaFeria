export default [
  {
    descripcion: 'Efectivo',
    pagoParcial: 0,
    interes: 0,
    pagoFinal: 0
  },
  {
    descripcion: 'Débito',
    pagoParcial: 0,
    interes: 0,
    pagoFinal: 0
  },
  {
    descripcion: 'Recargo 1',
    pagoParcial: 0,
    interes: 10,
    pagoFinal: 0
  },
  {
    descripcion: 'Recargo 2',
    pagoParcial: 0,
    interes: 15,
    pagoFinal: 0
  },
  {
    descripcion: 'Recargo 3',
    pagoParcial: 0,
    interes: 20,
    pagoFinal: 0
  }
];
