export default {
  ultimasJornadas: [
    {
      'id': 12,
      'tipo': 'jornada',
      'fecha': 'Sun Jul 20 1975 06:25:31 GMT+0000 (UTC)',
      'montoEfectivo': 8,
      'montoTarjeta': 6,
      'detalles': []
    },
    {
      'id': 1,
      'tipo': 'jornada',
      'fecha': 'Sun Jan 08 1984 12:46:10 GMT+0000 (UTC)',
      'montoEfectivo': 12,
      'montoTarjeta': 1,
      'detalles': []
    },
    {
      'id': 2,
      'tipo': 'jornada',
      'fecha': 'Thu Oct 18 2007 22:18:34 GMT+0000 (UTC)',
      'montoEfectivo': 2,
      'montoTarjeta': 7,
      'detalles': []
    }
  ],
  meses: [
    {
      'id': 12,
      'tipo': 'mes',
      'fecha': 'Thu Jan 31 1980 01:41:57 GMT+0000 (UTC)',
      'montoEfectivo': 5,
      'montoTarjeta': 4,
      'detalles': []
    },
    {
      'id': 1,
      'tipo': 'mes',
      'fecha': 'Sat Sep 27 2003 22:38:03 GMT+0000 (UTC)',
      'montoEfectivo': 10,
      'montoTarjeta': 8,
      'detalles': []
    },
    {
      'id': 2,
      'tipo': 'mes',
      'fecha': 'Thu Sep 21 1989 01:18:06 GMT+0000 (UTC)',
      'montoEfectivo': 9,
      'montoTarjeta': 10,
      'detalles': []
    },
    {
      'id': 3,
      'tipo': 'mes',
      'fecha': 'Wed Nov 29 1989 03:41:51 GMT+0000 (UTC)',
      'montoEfectivo': 2,
      'montoTarjeta': 4,
      'detalles': []
    },
    {
      'id': 4,
      'tipo': 'mes',
      'fecha': 'Fri Oct 28 1994 10:13:25 GMT+0000 (UTC)',
      'montoEfectivo': 2,
      'montoTarjeta': 10,
      'detalles': []
    },
    {
      'id': 5,
      'tipo': 'mes',
      'fecha': 'Thu Oct 05 2000 18:34:02 GMT+0000 (UTC)',
      'montoEfectivo': 1,
      'montoTarjeta': 6,
      'detalles': []
    },
    {
      'id': 6,
      'tipo': 'mes',
      'fecha': 'Sun Jun 20 1971 12:34:33 GMT+0000 (UTC)',
      'montoEfectivo': 12,
      'montoTarjeta': 12,
      'detalles': []
    },
    {
      'id': 7,
      'tipo': 'mes',
      'fecha': 'Tue Apr 04 1972 22:40:58 GMT+0000 (UTC)',
      'montoEfectivo': 4,
      'montoTarjeta': 5,
      'detalles': []
    },
    {
      'id': 8,
      'tipo': 'mes',
      'fecha': 'Fri Aug 29 1975 07:04:57 GMT+0000 (UTC)',
      'montoEfectivo': 6,
      'montoTarjeta': 2,
      'detalles': []
    },
    {
      'id': 9,
      'tipo': 'mes',
      'fecha': 'Tue May 29 1990 21:18:54 GMT+0000 (UTC)',
      'montoEfectivo': 2,
      'montoTarjeta': 5,
      'detalles': []
    },
    {
      'id': 10,
      'tipo': 'mes',
      'fecha': 'Wed Nov 21 1984 09:43:45 GMT+0000 (UTC)',
      'montoEfectivo': 9,
      'montoTarjeta': 3,
      'detalles': []
    },
    {
      'id': 11,
      'tipo': 'mes',
      'fecha': 'Fri Apr 07 1972 09:28:52 GMT+0000 (UTC)',
      'montoEfectivo': 1,
      'montoTarjeta': 4,
      'detalles': []
    }
  ],
  detallesMes: {
    'id': 12,
    'tipo': 'mes',
    'fecha': 'Sat Dec 16 2017 13:14:06 GMT+0000 (UTC)',
    'montoEfectivo': 6,
    'montoTarjeta': 6,
    'detalles': [
      {
        'id': 12,
        'tipo': 'jornada',
        'fecha': 'Tue Sep 10 1974 03:22:58 GMT+0000 (UTC)',
        'montoEfectivo': 9,
        'montoTarjeta': 3,
        'detalles': []
      },
      {
        'id': 1,
        'tipo': 'jornada',
        'fecha': 'Thu Jan 05 2006 15:23:04 GMT+0000 (UTC)',
        'montoEfectivo': 7,
        'montoTarjeta': 2,
        'detalles': []
      },
      {
        'id': 2,
        'tipo': 'jornada',
        'fecha': 'Mon Jun 05 1995 08:06:09 GMT+0000 (UTC)',
        'montoEfectivo': 9,
        'montoTarjeta': 2,
        'detalles': []
      },
      {
        'id': 3,
        'tipo': 'jornada',
        'fecha': 'Thu Dec 10 1998 17:56:31 GMT+0000 (UTC)',
        'montoEfectivo': 4,
        'montoTarjeta': 7,
        'detalles': []
      },
      {
        'id': 4,
        'tipo': 'jornada',
        'fecha': 'Sat Feb 18 1978 05:05:13 GMT+0000 (UTC)',
        'montoEfectivo': 8,
        'montoTarjeta': 5,
        'detalles': []
      },
      {
        'id': 5,
        'tipo': 'jornada',
        'fecha': 'Tue May 30 2006 04:04:33 GMT+0000 (UTC)',
        'montoEfectivo': 5,
        'montoTarjeta': 9,
        'detalles': []
      },
      {
        'id': 6,
        'tipo': 'jornada',
        'fecha': 'Fri Oct 22 1993 14:13:23 GMT+0000 (UTC)',
        'montoEfectivo': 12,
        'montoTarjeta': 9,
        'detalles': []
      },
      {
        'id': 7,
        'tipo': 'jornada',
        'fecha': 'Wed Sep 06 1978 19:05:00 GMT+0000 (UTC)',
        'montoEfectivo': 6,
        'montoTarjeta': 2,
        'detalles': []
      },
      {
        'id': 8,
        'tipo': 'jornada',
        'fecha': 'Wed Jan 11 1995 18:49:59 GMT+0000 (UTC)',
        'montoEfectivo': 7,
        'montoTarjeta': 7,
        'detalles': []
      },
      {
        'id': 9,
        'tipo': 'jornada',
        'fecha': 'Fri Apr 22 1977 08:53:22 GMT+0000 (UTC)',
        'montoEfectivo': 12,
        'montoTarjeta': 8,
        'detalles': []
      },
      {
        'id': 10,
        'tipo': 'jornada',
        'fecha': 'Tue Nov 23 1993 19:50:29 GMT+0000 (UTC)',
        'montoEfectivo': 2,
        'montoTarjeta': 10,
        'detalles': []
      },
      {
        'id': 11,
        'tipo': 'jornada',
        'fecha': 'Sun Feb 03 1974 12:31:31 GMT+0000 (UTC)',
        'montoEfectivo': 3,
        'montoTarjeta': 6,
        'detalles': []
      },
      {
        'id': 12,
        'tipo': 'jornada',
        'fecha': 'Sat Jun 05 1982 06:53:44 GMT+0000 (UTC)',
        'montoEfectivo': 7,
        'montoTarjeta': 10,
        'detalles': []
      },
      {
        'id': 13,
        'tipo': 'jornada',
        'fecha': 'Fri May 11 2001 14:23:00 GMT+0000 (UTC)',
        'montoEfectivo': 2,
        'montoTarjeta': 9,
        'detalles': []
      },
      {
        'id': 14,
        'tipo': 'jornada',
        'fecha': 'Fri Feb 19 1988 22:38:56 GMT+0000 (UTC)',
        'montoEfectivo': 4,
        'montoTarjeta': 12,
        'detalles': []
      },
      {
        'id': 15,
        'tipo': 'jornada',
        'fecha': 'Mon Mar 22 2010 02:26:30 GMT+0000 (UTC)',
        'montoEfectivo': 10,
        'montoTarjeta': 1,
        'detalles': []
      },
      {
        'id': 16,
        'tipo': 'jornada',
        'fecha': 'Tue Jul 12 1977 14:42:03 GMT+0000 (UTC)',
        'montoEfectivo': 10,
        'montoTarjeta': 9,
        'detalles': []
      },
      {
        'id': 17,
        'tipo': 'jornada',
        'fecha': 'Mon Aug 09 2010 12:36:56 GMT+0000 (UTC)',
        'montoEfectivo': 5,
        'montoTarjeta': 5,
        'detalles': []
      },
      {
        'id': 18,
        'tipo': 'jornada',
        'fecha': 'Sun Feb 23 1975 01:02:12 GMT+0000 (UTC)',
        'montoEfectivo': 8,
        'montoTarjeta': 8,
        'detalles': []
      },
      {
        'id': 19,
        'tipo': 'jornada',
        'fecha': 'Wed Sep 02 2009 05:31:54 GMT+0000 (UTC)',
        'montoEfectivo': 2,
        'montoTarjeta': 4,
        'detalles': []
      },
      {
        'id': 20,
        'tipo': 'jornada',
        'fecha': 'Tue Dec 09 2014 21:03:19 GMT+0000 (UTC)',
        'montoEfectivo': 12,
        'montoTarjeta': 6,
        'detalles': []
      },
      {
        'id': 21,
        'tipo': 'jornada',
        'fecha': 'Sat Jun 04 1977 13:08:52 GMT+0000 (UTC)',
        'montoEfectivo': 10,
        'montoTarjeta': 10,
        'detalles': []
      },
      {
        'id': 22,
        'tipo': 'jornada',
        'fecha': 'Wed Jul 08 1987 14:34:54 GMT+0000 (UTC)',
        'montoEfectivo': 12,
        'montoTarjeta': 2,
        'detalles': []
      },
      {
        'id': 23,
        'tipo': 'jornada',
        'fecha': 'Sat Sep 03 1994 01:29:21 GMT+0000 (UTC)',
        'montoEfectivo': 12,
        'montoTarjeta': 2,
        'detalles': []
      },
      {
        'id': 24,
        'tipo': 'jornada',
        'fecha': 'Sun Sep 30 1979 02:23:45 GMT+0000 (UTC)',
        'montoEfectivo': 7,
        'montoTarjeta': 9,
        'detalles': []
      },
      {
        'id': 25,
        'tipo': 'jornada',
        'fecha': 'Tue Nov 22 1988 17:54:41 GMT+0000 (UTC)',
        'montoEfectivo': 10,
        'montoTarjeta': 5,
        'detalles': []
      },
      {
        'id': 26,
        'tipo': 'jornada',
        'fecha': 'Thu Feb 13 2014 15:02:43 GMT+0000 (UTC)',
        'montoEfectivo': 3,
        'montoTarjeta': 12,
        'detalles': []
      },
      {
        'id': 27,
        'tipo': 'jornada',
        'fecha': 'Wed May 07 1975 06:27:58 GMT+0000 (UTC)',
        'montoEfectivo': 12,
        'montoTarjeta': 7,
        'detalles': []
      },
      {
        'id': 28,
        'tipo': 'jornada',
        'fecha': 'Sun Oct 02 1988 06:53:24 GMT+0000 (UTC)',
        'montoEfectivo': 7,
        'montoTarjeta': 12,
        'detalles': []
      },
      {
        'id': 29,
        'tipo': 'jornada',
        'fecha': 'Mon Jul 02 1984 23:53:37 GMT+0000 (UTC)',
        'montoEfectivo': 12,
        'montoTarjeta': 2,
        'detalles': []
      }
    ]
  },
  detallesJornada: {
    'id': 12,
    'tipo': 'jornada',
    'fecha': 'Thu Jan 17 1974 11:20:30 GMT+0000 (UTC)',
    'montoEfectivo': 9,
    'montoTarjeta': 10,
    'detalles': [
      {
        'id': 12,
        'tipo': 'venta',
        'fecha': 'Thu Nov 15 1990 00:06:36 GMT+0000 (UTC)',
        'montoEfectivo': 2,
        'montoTarjeta': 5,
        'detalles': []
      },
      {
        'id': 1,
        'tipo': 'venta',
        'fecha': 'Sun May 28 2000 03:37:15 GMT+0000 (UTC)',
        'montoEfectivo': 12,
        'montoTarjeta': 4,
        'detalles': []
      },
      {
        'id': 2,
        'tipo': 'venta',
        'fecha': 'Thu Mar 31 1988 22:34:07 GMT+0000 (UTC)',
        'montoEfectivo': 10,
        'montoTarjeta': 6,
        'detalles': []
      },
      {
        'id': 3,
        'tipo': 'venta',
        'fecha': 'Fri Jun 11 2004 14:13:33 GMT+0000 (UTC)',
        'montoEfectivo': 3,
        'montoTarjeta': 1,
        'detalles': []
      },
      {
        'id': 4,
        'tipo': 'venta',
        'fecha': 'Wed Jul 30 1980 01:00:59 GMT+0000 (UTC)',
        'montoEfectivo': 2,
        'montoTarjeta': 6,
        'detalles': []
      },
      {
        'id': 5,
        'tipo': 'venta',
        'fecha': 'Thu Dec 01 2005 10:21:44 GMT+0000 (UTC)',
        'montoEfectivo': 6,
        'montoTarjeta': 9,
        'detalles': []
      },
      {
        'id': 6,
        'tipo': 'venta',
        'fecha': 'Fri Jun 28 1974 20:21:00 GMT+0000 (UTC)',
        'montoEfectivo': 12,
        'montoTarjeta': 5,
        'detalles': []
      },
      {
        'id': 7,
        'tipo': 'venta',
        'fecha': 'Thu Oct 12 1995 18:45:32 GMT+0000 (UTC)',
        'montoEfectivo': 6,
        'montoTarjeta': 5,
        'detalles': []
      },
      {
        'id': 8,
        'tipo': 'venta',
        'fecha': 'Fri Mar 24 2000 12:29:38 GMT+0000 (UTC)',
        'montoEfectivo': 2,
        'montoTarjeta': 8,
        'detalles': []
      },
      {
        'id': 9,
        'tipo': 'venta',
        'fecha': 'Thu Oct 09 1997 00:19:48 GMT+0000 (UTC)',
        'montoEfectivo': 10,
        'montoTarjeta': 6,
        'detalles': []
      },
      {
        'id': 10,
        'tipo': 'venta',
        'fecha': 'Tue Sep 29 2009 07:16:39 GMT+0000 (UTC)',
        'montoEfectivo': 4,
        'montoTarjeta': 12,
        'detalles': []
      },
      {
        'id': 11,
        'tipo': 'venta',
        'fecha': 'Tue Sep 19 2000 16:25:51 GMT+0000 (UTC)',
        'montoEfectivo': 8,
        'montoTarjeta': 2,
        'detalles': []
      },
      {
        'id': 12,
        'tipo': 'venta',
        'fecha': 'Sun Feb 13 2005 15:11:01 GMT+0000 (UTC)',
        'montoEfectivo': 7,
        'montoTarjeta': 3,
        'detalles': []
      },
      {
        'id': 13,
        'tipo': 'venta',
        'fecha': 'Fri Sep 10 2010 09:17:46 GMT+0000 (UTC)',
        'montoEfectivo': 6,
        'montoTarjeta': 12,
        'detalles': []
      },
      {
        'id': 14,
        'tipo': 'venta',
        'fecha': 'Mon Mar 03 1980 19:59:02 GMT+0000 (UTC)',
        'montoEfectivo': 3,
        'montoTarjeta': 5,
        'detalles': []
      },
      {
        'id': 15,
        'tipo': 'venta',
        'fecha': 'Sat Jun 11 1977 03:02:18 GMT+0000 (UTC)',
        'montoEfectivo': 1,
        'montoTarjeta': 9,
        'detalles': []
      },
      {
        'id': 16,
        'tipo': 'venta',
        'fecha': 'Sat Aug 10 1996 11:18:25 GMT+0000 (UTC)',
        'montoEfectivo': 4,
        'montoTarjeta': 3,
        'detalles': []
      },
      {
        'id': 17,
        'tipo': 'venta',
        'fecha': 'Tue Sep 19 1989 15:35:34 GMT+0000 (UTC)',
        'montoEfectivo': 10,
        'montoTarjeta': 6,
        'detalles': []
      },
      {
        'id': 18,
        'tipo': 'venta',
        'fecha': 'Wed Oct 23 2002 10:59:24 GMT+0000 (UTC)',
        'montoEfectivo': 8,
        'montoTarjeta': 5,
        'detalles': []
      },
      {
        'id': 19,
        'tipo': 'venta',
        'fecha': 'Mon Jul 15 1996 01:39:15 GMT+0000 (UTC)',
        'montoEfectivo': 8,
        'montoTarjeta': 2,
        'detalles': []
      },
      {
        'id': 20,
        'tipo': 'venta',
        'fecha': 'Fri Aug 11 1972 14:10:52 GMT+0000 (UTC)',
        'montoEfectivo': 7,
        'montoTarjeta': 1,
        'detalles': []
      },
      {
        'id': 21,
        'tipo': 'venta',
        'fecha': 'Sat Oct 03 1992 01:50:31 GMT+0000 (UTC)',
        'montoEfectivo': 9,
        'montoTarjeta': 3,
        'detalles': []
      },
      {
        'id': 22,
        'tipo': 'venta',
        'fecha': 'Fri Sep 17 1999 12:31:04 GMT+0000 (UTC)',
        'montoEfectivo': 10,
        'montoTarjeta': 7,
        'detalles': []
      },
      {
        'id': 23,
        'tipo': 'venta',
        'fecha': 'Fri Mar 26 2004 15:11:45 GMT+0000 (UTC)',
        'montoEfectivo': 2,
        'montoTarjeta': 6,
        'detalles': []
      },
      {
        'id': 24,
        'tipo': 'venta',
        'fecha': 'Tue Mar 24 1970 05:54:45 GMT+0000 (UTC)',
        'montoEfectivo': 2,
        'montoTarjeta': 5,
        'detalles': []
      },
      {
        'id': 25,
        'tipo': 'venta',
        'fecha': 'Thu Mar 28 1974 03:01:15 GMT+0000 (UTC)',
        'montoEfectivo': 10,
        'montoTarjeta': 7,
        'detalles': []
      },
      {
        'id': 26,
        'tipo': 'venta',
        'fecha': 'Mon Dec 09 1974 05:54:32 GMT+0000 (UTC)',
        'montoEfectivo': 3,
        'montoTarjeta': 3,
        'detalles': []
      },
      {
        'id': 27,
        'tipo': 'venta',
        'fecha': 'Thu Jun 01 2006 13:07:03 GMT+0000 (UTC)',
        'montoEfectivo': 10,
        'montoTarjeta': 8,
        'detalles': []
      },
      {
        'id': 28,
        'tipo': 'venta',
        'fecha': 'Sun May 17 1970 11:00:22 GMT+0000 (UTC)',
        'montoEfectivo': 8,
        'montoTarjeta': 7,
        'detalles': []
      },
      {
        'id': 29,
        'tipo': 'venta',
        'fecha': 'Sun Sep 07 2014 07:19:33 GMT+0000 (UTC)',
        'montoEfectivo': 8,
        'montoTarjeta': 3,
        'detalles': []
      },
      {
        'id': 30,
        'tipo': 'venta',
        'fecha': 'Wed Nov 17 1971 08:49:48 GMT+0000 (UTC)',
        'montoEfectivo': 4,
        'montoTarjeta': 7,
        'detalles': []
      },
      {
        'id': 31,
        'tipo': 'venta',
        'fecha': 'Mon Dec 06 1993 13:40:55 GMT+0000 (UTC)',
        'montoEfectivo': 1,
        'montoTarjeta': 12,
        'detalles': []
      },
      {
        'id': 32,
        'tipo': 'venta',
        'fecha': 'Wed Oct 31 1984 02:11:16 GMT+0000 (UTC)',
        'montoEfectivo': 7,
        'montoTarjeta': 9,
        'detalles': []
      },
      {
        'id': 33,
        'tipo': 'venta',
        'fecha': 'Fri Nov 01 1996 06:57:58 GMT+0000 (UTC)',
        'montoEfectivo': 3,
        'montoTarjeta': 1,
        'detalles': []
      },
      {
        'id': 34,
        'tipo': 'venta',
        'fecha': 'Tue Oct 20 1970 21:04:23 GMT+0000 (UTC)',
        'montoEfectivo': 7,
        'montoTarjeta': 9,
        'detalles': []
      },
      {
        'id': 35,
        'tipo': 'venta',
        'fecha': 'Mon Mar 19 2007 09:51:41 GMT+0000 (UTC)',
        'montoEfectivo': 10,
        'montoTarjeta': 3,
        'detalles': []
      },
      {
        'id': 36,
        'tipo': 'venta',
        'fecha': 'Fri Mar 06 1987 18:38:41 GMT+0000 (UTC)',
        'montoEfectivo': 6,
        'montoTarjeta': 10,
        'detalles': []
      },
      {
        'id': 37,
        'tipo': 'venta',
        'fecha': 'Mon Sep 26 1983 15:20:57 GMT+0000 (UTC)',
        'montoEfectivo': 2,
        'montoTarjeta': 2,
        'detalles': []
      },
      {
        'id': 38,
        'tipo': 'venta',
        'fecha': 'Tue Sep 05 2006 11:15:05 GMT+0000 (UTC)',
        'montoEfectivo': 12,
        'montoTarjeta': 1,
        'detalles': []
      },
      {
        'id': 39,
        'tipo': 'venta',
        'fecha': 'Mon Nov 12 1979 14:57:09 GMT+0000 (UTC)',
        'montoEfectivo': 6,
        'montoTarjeta': 2,
        'detalles': []
      },
      {
        'id': 40,
        'tipo': 'venta',
        'fecha': 'Fri Jun 21 2013 21:11:05 GMT+0000 (UTC)',
        'montoEfectivo': 10,
        'montoTarjeta': 8,
        'detalles': []
      },
      {
        'id': 41,
        'tipo': 'venta',
        'fecha': 'Thu Jun 25 2009 21:44:59 GMT+0000 (UTC)',
        'montoEfectivo': 10,
        'montoTarjeta': 2,
        'detalles': []
      },
      {
        'id': 42,
        'tipo': 'venta',
        'fecha': 'Tue Jul 16 1985 20:34:08 GMT+0000 (UTC)',
        'montoEfectivo': 5,
        'montoTarjeta': 3,
        'detalles': []
      },
      {
        'id': 43,
        'tipo': 'venta',
        'fecha': 'Wed Sep 01 1993 17:29:16 GMT+0000 (UTC)',
        'montoEfectivo': 3,
        'montoTarjeta': 3,
        'detalles': []
      },
      {
        'id': 44,
        'tipo': 'venta',
        'fecha': 'Thu Feb 24 1972 16:14:14 GMT+0000 (UTC)',
        'montoEfectivo': 1,
        'montoTarjeta': 8,
        'detalles': []
      },
      {
        'id': 45,
        'tipo': 'venta',
        'fecha': 'Sat Jan 23 2010 10:14:57 GMT+0000 (UTC)',
        'montoEfectivo': 2,
        'montoTarjeta': 4,
        'detalles': []
      },
      {
        'id': 46,
        'tipo': 'venta',
        'fecha': 'Thu Dec 13 2007 22:02:59 GMT+0000 (UTC)',
        'montoEfectivo': 3,
        'montoTarjeta': 1,
        'detalles': []
      },
      {
        'id': 47,
        'tipo': 'venta',
        'fecha': 'Sun May 03 1998 00:00:43 GMT+0000 (UTC)',
        'montoEfectivo': 8,
        'montoTarjeta': 5,
        'detalles': []
      },
      {
        'id': 48,
        'tipo': 'venta',
        'fecha': 'Mon Jul 16 1984 07:08:33 GMT+0000 (UTC)',
        'montoEfectivo': 6,
        'montoTarjeta': 1,
        'detalles': []
      },
      {
        'id': 49,
        'tipo': 'venta',
        'fecha': 'Tue Oct 13 1998 18:14:04 GMT+0000 (UTC)',
        'montoEfectivo': 7,
        'montoTarjeta': 4,
        'detalles': []
      },
      {
        'id': 50,
        'tipo': 'venta',
        'fecha': 'Sun Sep 25 1994 20:55:56 GMT+0000 (UTC)',
        'montoEfectivo': 10,
        'montoTarjeta': 12,
        'detalles': []
      },
      {
        'id': 51,
        'tipo': 'venta',
        'fecha': 'Sun Jan 13 1991 05:28:20 GMT+0000 (UTC)',
        'montoEfectivo': 10,
        'montoTarjeta': 2,
        'detalles': []
      },
      {
        'id': 52,
        'tipo': 'venta',
        'fecha': 'Wed Apr 07 1982 16:55:31 GMT+0000 (UTC)',
        'montoEfectivo': 2,
        'montoTarjeta': 3,
        'detalles': []
      },
      {
        'id': 53,
        'tipo': 'venta',
        'fecha': 'Mon Aug 17 1998 00:22:09 GMT+0000 (UTC)',
        'montoEfectivo': 8,
        'montoTarjeta': 7,
        'detalles': []
      },
      {
        'id': 54,
        'tipo': 'venta',
        'fecha': 'Sun Aug 19 2012 15:11:53 GMT+0000 (UTC)',
        'montoEfectivo': 4,
        'montoTarjeta': 2,
        'detalles': []
      },
      {
        'id': 55,
        'tipo': 'venta',
        'fecha': 'Thu Oct 22 1992 20:44:13 GMT+0000 (UTC)',
        'montoEfectivo': 2,
        'montoTarjeta': 5,
        'detalles': []
      },
      {
        'id': 56,
        'tipo': 'venta',
        'fecha': 'Wed Sep 17 1980 21:01:16 GMT+0000 (UTC)',
        'montoEfectivo': 12,
        'montoTarjeta': 1,
        'detalles': []
      },
      {
        'id': 57,
        'tipo': 'venta',
        'fecha': 'Wed Jun 01 1977 16:49:32 GMT+0000 (UTC)',
        'montoEfectivo': 5,
        'montoTarjeta': 10,
        'detalles': []
      },
      {
        'id': 58,
        'tipo': 'venta',
        'fecha': 'Mon Mar 30 1992 04:30:14 GMT+0000 (UTC)',
        'montoEfectivo': 1,
        'montoTarjeta': 5,
        'detalles': []
      },
      {
        'id': 59,
        'tipo': 'venta',
        'fecha': 'Wed Mar 05 2008 07:34:02 GMT+0000 (UTC)',
        'montoEfectivo': 2,
        'montoTarjeta': 10,
        'detalles': []
      },
      {
        'id': 60,
        'tipo': 'venta',
        'fecha': 'Fri Feb 03 1995 10:50:31 GMT+0000 (UTC)',
        'montoEfectivo': 4,
        'montoTarjeta': 7,
        'detalles': []
      },
      {
        'id': 61,
        'tipo': 'venta',
        'fecha': 'Mon Nov 11 2002 01:01:26 GMT+0000 (UTC)',
        'montoEfectivo': 1,
        'montoTarjeta': 8,
        'detalles': []
      },
      {
        'id': 62,
        'tipo': 'venta',
        'fecha': 'Sat Aug 30 1975 00:08:59 GMT+0000 (UTC)',
        'montoEfectivo': 2,
        'montoTarjeta': 10,
        'detalles': []
      },
      {
        'id': 63,
        'tipo': 'venta',
        'fecha': 'Mon Apr 01 1985 19:04:37 GMT+0000 (UTC)',
        'montoEfectivo': 8,
        'montoTarjeta': 6,
        'detalles': []
      },
      {
        'id': 64,
        'tipo': 'venta',
        'fecha': 'Sat May 10 2003 05:25:13 GMT+0000 (UTC)',
        'montoEfectivo': 8,
        'montoTarjeta': 5,
        'detalles': []
      },
      {
        'id': 65,
        'tipo': 'venta',
        'fecha': 'Sat Nov 05 1977 19:52:20 GMT+0000 (UTC)',
        'montoEfectivo': 6,
        'montoTarjeta': 5,
        'detalles': []
      },
      {
        'id': 66,
        'tipo': 'venta',
        'fecha': 'Sat Oct 26 2013 06:22:49 GMT+0000 (UTC)',
        'montoEfectivo': 3,
        'montoTarjeta': 10,
        'detalles': []
      },
      {
        'id': 67,
        'tipo': 'venta',
        'fecha': 'Tue Dec 06 2005 04:48:44 GMT+0000 (UTC)',
        'montoEfectivo': 3,
        'montoTarjeta': 1,
        'detalles': []
      },
      {
        'id': 68,
        'tipo': 'venta',
        'fecha': 'Sun Dec 20 2015 06:27:42 GMT+0000 (UTC)',
        'montoEfectivo': 3,
        'montoTarjeta': 1,
        'detalles': []
      },
      {
        'id': 69,
        'tipo': 'venta',
        'fecha': 'Tue Aug 19 2014 03:42:35 GMT+0000 (UTC)',
        'montoEfectivo': 3,
        'montoTarjeta': 9,
        'detalles': []
      },
      {
        'id': 70,
        'tipo': 'venta',
        'fecha': 'Fri Oct 10 2003 20:07:05 GMT+0000 (UTC)',
        'montoEfectivo': 7,
        'montoTarjeta': 1,
        'detalles': []
      },
      {
        'id': 71,
        'tipo': 'venta',
        'fecha': 'Mon Sep 23 1991 23:18:00 GMT+0000 (UTC)',
        'montoEfectivo': 7,
        'montoTarjeta': 10,
        'detalles': []
      },
      {
        'id': 72,
        'tipo': 'venta',
        'fecha': 'Sat Jul 27 1985 00:20:43 GMT+0000 (UTC)',
        'montoEfectivo': 5,
        'montoTarjeta': 12,
        'detalles': []
      },
      {
        'id': 73,
        'tipo': 'venta',
        'fecha': 'Thu Dec 27 1990 11:42:47 GMT+0000 (UTC)',
        'montoEfectivo': 8,
        'montoTarjeta': 10,
        'detalles': []
      },
      {
        'id': 74,
        'tipo': 'venta',
        'fecha': 'Tue Jan 14 2003 16:47:09 GMT+0000 (UTC)',
        'montoEfectivo': 7,
        'montoTarjeta': 5,
        'detalles': []
      },
      {
        'id': 75,
        'tipo': 'venta',
        'fecha': 'Mon Apr 02 2012 21:58:01 GMT+0000 (UTC)',
        'montoEfectivo': 1,
        'montoTarjeta': 9,
        'detalles': []
      },
      {
        'id': 76,
        'tipo': 'venta',
        'fecha': 'Wed May 05 1971 10:02:57 GMT+0000 (UTC)',
        'montoEfectivo': 6,
        'montoTarjeta': 10,
        'detalles': []
      },
      {
        'id': 77,
        'tipo': 'venta',
        'fecha': 'Tue Sep 17 2013 07:36:48 GMT+0000 (UTC)',
        'montoEfectivo': 9,
        'montoTarjeta': 12,
        'detalles': []
      },
      {
        'id': 78,
        'tipo': 'venta',
        'fecha': 'Wed Oct 04 1978 09:50:24 GMT+0000 (UTC)',
        'montoEfectivo': 9,
        'montoTarjeta': 6,
        'detalles': []
      },
      {
        'id': 79,
        'tipo': 'venta',
        'fecha': 'Sun Nov 16 1980 06:07:49 GMT+0000 (UTC)',
        'montoEfectivo': 5,
        'montoTarjeta': 2,
        'detalles': []
      }
    ]
  }
}
