export default [
  {
    'id': 0,
    'nombre': 'Administrador de Prueba',
    'pass': 'test',
    'passComparar': 'test',
    'tipoUsuario': 'Administrador',
    'estado': true,
    'creadoEl': 'Tue Sep 23 1997 09:36:09 GMT+0000 (UTC)',
    'bloqueadoEl': 'Sat Oct 27 1973 22:19:36 GMT+0000 (UTC)',
    'sueldo': 1876,
    'ultimoAcceso': 'Sat Oct 06 1984 11:49:01 GMT+0000 (UTC)',
    'editando': false
  },
  {
    'id': 0,
    'nombre': 'Vendedor de Prueba',
    'pass': 'idea',
    'passComparar': 'idea',
    'tipoUsuario': 'Vendedor',
    'estado': true,
    'creadoEl': 'Tue Sep 23 1997 09:36:09 GMT+0000 (UTC)',
    'bloqueadoEl': 'Sat Oct 27 1973 22:19:36 GMT+0000 (UTC)',
    'sueldo': 1876,
    'ultimoAcceso': 'Sat Oct 06 1984 11:49:01 GMT+0000 (UTC)',
    'editando': false
  }
];
