// IMPORTAR TIPOS
import { Fecha } from './tipos/fecha';
// IMPORTAR ENTIDADES
import { Articulo } from './articulo';
import { MedioPago } from './medioPago';

const ventaDummy = {
  pagos: []
};

export class Venta {
  articulos: Articulo[];
  pagos: MedioPago[];
  fechaVenta: Fecha;
  vendedor: string;
  // VARIABLES NO ENVIADAS A BE
  $key: string;
  id: any;
  mostrar: boolean;

  constructor(_obj: any = ventaDummy) {
    if(_obj.articulos) {
      _obj.articulos =
        _obj.articulos.map(
          (_art: any) => {
            return new Articulo(_art); }
        );
    } else {
      _obj.articulos = [new Articulo(null)]
    }

    if(_obj.pagos) {
      _obj.pagos =
        _obj.pagos.map(
          (_pago: any) => {
            return new MedioPago(_pago)
          }
        );
    } else {
      _obj.pagos = [];
    }

    this.articulos = _obj.articulos;
    this.pagos = _obj.pagos;
    this.fechaVenta = new Fecha(_obj.fechaVenta);
    this.vendedor = _obj.vendedor || null;
    // VARIABLES NO ENVIADAS A BE
    this.$key = _obj.$key;
    this.id = _obj.id || 0;
    this.mostrar = true;
  }

  get precioArticulos(): number {
    let _precio: number = 0;

    this.articulos.forEach(
      art => _precio += Number(art.calcularPrecioVenta)
    );

    return _precio;
  }

  get valoresIguales(): boolean {
    let _totalArticulos: number = 0;
    let _totalMedioPago: number = 0;

    this.articulos.forEach(
      art => _totalArticulos += this.formatearNumero(art.calcularPrecioVenta)
    );

    this.pagos.forEach(
      pago => _totalMedioPago += this.formatearNumero(pago.pagoParcial.valor)
    );

    return _totalArticulos === _totalMedioPago;
  }

  get sonArticulosValidos(): boolean {
    const _listaCant = this.articulos.length;
    const _existeUnArticulo = _listaCant === 1 && this.articulos[0].esValido;
    const _articulosValidos =
      _listaCant > 1 &&
      this.articulos.filter(
        (art, i) => {
          return (i !== (_listaCant - 1)) ? !art.esValido : false;
        }
      ).length === 0;

    return _existeUnArticulo || _articulosValidos;
  }

  get esValida(): boolean {
    return this.sonArticulosValidos && this.valoresIguales;
  }

  get textoArticulos(): string[] {
    return this.articulos.map(
      _art => {
        return `${_art.codigo.valor} -  ${_art.descripcion.valor} - $${_art.calcularPrecioVenta}`;
      }
    );
  }

  get totalEfectivo(): number {
    let efectivo;

    switch(this.pagos[0].pagoParcial.valor) {
      case null:
      case undefined: {
        efectivo = 0;
        break;
      }
      default: {
        efectivo = +(this.pagos[0].pagoParcial.valor);
        break;
      }
    }
    return efectivo;
  }

  get totalTarjetas(): number {
    let _total: number = 0;

    this.pagos.forEach(
      (_pago, i) => {
        if (i > 0) {
          _total += +(_pago.pagoParcial.valor || 0);
        }
      }
    )

    return _total;
  }

  sumarTotal(): void {
    this.pagos.forEach(
      p => p.pagoParcial.valor = 0
    );

    this.articulos.forEach(
      art => {
        this.pagos[0].pagoParcial.valor += this.formatearNumero(art.calcularPrecioVenta);
      }
    );
  }

  agregarArticulo() {
    const ultimoArt = this.articulos.slice(this.articulos.length - 1)[0];

    if (ultimoArt.codigo.esValido) {
      this.articulos.push(new Articulo(null));
    }
  }

  removerArticulo(i: number) {
    this.articulos.splice(i, 1);
  }

  calcularTotal(esParcial: boolean): number {
    let _total: number = 0;

    this.pagos.map(
      p => {
        _total += esParcial ?
          this.formatearNumero(p.pagoParcial.valor) : this.formatearNumero(p.calcularPago);
      });

    return _total;
  }

  limpiarVenta(): void {
    this.articulos = [new Articulo(null)];
  }

  filtarArticulosNulos() {
    this.articulos.forEach(
      (_art, i) => {
        if (!_art.codigo.esValido && !_art.descripcion.esValido) {
          this.articulos.splice(i, 1)
        }
      }
    )
  }

  toJS() {
    this.filtarArticulosNulos();
    this.fechaVenta.valor = new Date().toISOString();

    const _articulos = this.articulos.map(
      _art => _art.toJS()
    )
    const _pagos = this.pagos.map(
      _pago => _pago.toJS()
    )

    return {
      'articulos': _articulos,
      'pagos': _pagos,
      'fechaVenta': this.fechaVenta.valor,
      'vendedor': this.vendedor
    }
  }

  private formatearNumero(numero: any): number {
    let _valor = Number(numero);
    return !isNaN(_valor) ? +_valor.toFixed(1) : 0;
  }
}
