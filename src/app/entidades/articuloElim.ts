export class ArticuloEliminado {
  codigo: string;
  descripcion: string;
  precioVenta: string;
  fechaEliminado: string;
  autorizacion: string;

  constructor(_obj: any) {
    this.codigo = _obj.codigo;
    this.descripcion = _obj.descripcion;
    this.precioVenta = _obj.precioVenta;
    this.fechaEliminado = _obj.fechaEliminado;
    this.autorizacion = _obj.autorizacion;
  }

  get darCodigoNombre(): string {
    return `${this.codigo} - ${this.descripcion}`;
  }

  toJS() {
    return {
      'codigo': this.codigo,
      'descripcion': this.descripcion,
      'precioVenta': this.precioVenta,
      'fechaEliminado': this.fechaEliminado,
      'autorizacion': this.autorizacion
    }
  }
}
