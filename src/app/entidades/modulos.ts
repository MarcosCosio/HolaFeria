class Modulo {
  raiz: boolean = false;
  crear: boolean = false;
  ver: boolean = false;
  modificar: boolean = false;
  eliminar: boolean = false;
  observar: boolean = false;
}

export class ModulosConfig {
  ventas: Modulo = new Modulo();
  stock: Modulo = new Modulo();
  estadisticas: Modulo = new Modulo();
  usuarios: Modulo = new Modulo();

  constructor() {}

  setearRuta(_ruta: any) {
    this.setearEstados(this, _ruta);
  }

  private setearEstados(_obj: any, _ruta: any) {
    const esRaiz = typeof _ruta === 'string';

    if (esRaiz) {
      _ruta = _ruta === '' ? 'ventas' : _ruta;

      for (const modulo in _obj) {
        if (_obj.hasOwnProperty(modulo)) {
          for (const accion in _obj[modulo]) {
            if (_obj[modulo].hasOwnProperty(accion)) {
              _obj[modulo][accion] = false;
            }
          }
        }
      }

      _obj[_ruta]['raiz'] = true;
    } else {
      const modulo = _ruta[0];
      const accion = _ruta[1];

      for (const p in _obj[modulo]) {
        if (_obj[modulo].hasOwnProperty(p)) {
          _obj[modulo][p] = false;
        }
      }
      _obj[modulo][accion] = true;
    }
  }
}
