import { Entidad } from './entidad';

export class Decimal extends Entidad {
  constructor(_valor: any) {
    super(_valor);
  }

  get esValido(): boolean {
    const _valor = Boolean(this.valor) && Number(this.valor);
    const _valorPrevio = Boolean(this.valorPrevio) && Number(this.valorPrevio);
    let _resultado = true;

    if (_valorPrevio) {
      _resultado = !isNaN(_valor) &&
        !isNaN(_valorPrevio) &&
        _valor >= _valorPrevio &&
        _valor <= 100
    } else {
      _resultado = Boolean(this.valor) &&
        !isNaN(_valor) &&
        _valor >= 0 &&
        _valor <= 100
    }

    return _resultado;
  }
}
