 export abstract class Entidad {
  valor: any;
  valorPrevio: any;

  constructor(_valor: any) {
    if (!_valor) {
      _valor = null;
    }

    this.valor = _valor;
    this.valorPrevio = _valor;
  }
}
