// IMPORTAR TIPOS
import { Numero } from './tipos/numero';
import { Fecha } from './tipos/fecha';
//IMPORTAR ENTIDADES
import { Descuento } from './descuento';
import { Usuario } from './usuario';

export class Jornada {
  id: any;
  cambio: Numero;
  hayDescuentos: String;
  listaDescuentos: Descuento[];
  listaVendedores: Usuario[];
  iniciadaPor: string;
  cerradaPor: string;
  montoEfectivo: number;
  montoTarjeta: number;
  // FECHAS
  fechaInicio: Fecha;
  fechaFin: Fecha;
  // VARIABLES NO ENVIADAS A BE
  $key: string;

  constructor(_obj: any) {
    if (!_obj) {
      _obj = {
        hayDescuentos: 'false',
        listaDescuentos: [new Descuento(null)],
        listaVendedores: [new Usuario()]
      };
    } else {
      _obj.listaDescuentos = _obj.hayDescuentos ?
        _obj.listaDescuentos.map(
          (_desc: any) => {
            return new Descuento(_desc);
          }) : [];

      _obj.listaVendedores = _obj.listaVendedores ?
        _obj.listaVendedores.map(
          (_vend: any) => {
            return new Usuario(_vend);
          }) : [];
    }

    this.id = _obj.id || 0;
    this.hayDescuentos = this.textoBooleano(_obj.hayDescuentos, false);
    this.listaDescuentos = _obj.listaDescuentos;
    this.listaVendedores = _obj.listaVendedores;
    this.cambio = new Numero(_obj.cambio);
    this.fechaInicio = new Fecha(_obj.fechaInicio);
    this.fechaFin = new Fecha(_obj.fechaFin);
    this.iniciadaPor = _obj.iniciadaPor || null;
    this.cerradaPor = _obj.cerradaPor || null;
    this.montoEfectivo = _obj.montoEfectivo || 0;
    this.montoTarjeta = _obj.montoTarjeta || 0;
    // VARIABLES NO ENVIADAS A BE
    this.$key = _obj.$key || null;
  }

  get tieneDescuento(): Boolean {
    return this.hayDescuentos !== null &&
      this.hayDescuentos === 'true';
  }

  get esValido(): Boolean {
    const _vendedoresValidos =
      this.listaVendedores.filter(
        _vend => {
          return _vend.nombre.esValido &&
            _vend.sueldo.esValido;
        }).length > 0;

    return this.cambio.esValido &&
      _vendedoresValidos;
  }

  get textoDescuentos(): string[] {
    return this.listaDescuentos.map(
      _desc => {
        return `${_desc.descripcion.valor}: ${_desc.descuento.valor}%`;
      }
    );
  }

  get fechaCorta(): string {
    const _fecha = this.fechaInicio.valor.split('-');
    return `${_fecha[0]}-${_fecha[1]}-${_fecha[2].split('T')[0]}`
  }

  get montoSueldos(): number {
    let _resultado: number = 0;

    this.listaVendedores.forEach(
      _vend => _resultado += (_vend.sueldo.valor * -1)
    );

    return _resultado;
  }

  get montoCaja(): number {
    return +this.cambio.valor + +this.montoEfectivo + +this.montoSueldos;
  }

  agregarDescuento() {
    this.listaDescuentos.push(new Descuento(null));
  }

  quitarDescuento(i: any) {
    this.listaDescuentos.splice(i, 1);
  }

  agregarVendedor() {
    this.listaVendedores.push(new Usuario());
  }

  quitarVendedor(i: any) {
    this.listaVendedores.splice(i, 1);
  }

  private textoBooleano(_valor: any, _aBooleano: Boolean) {
    if (_aBooleano) {
      return _valor === 'true';
    } else {
      return _valor.toString();
    }
  }

  toJS() {
    const _listaDescuentos = this.tieneDescuento ?
      this.listaDescuentos.map(
        (_desc: Descuento) => {
          return _desc.toJS();
        }) : [];

    this.listaVendedores.forEach(
      (_vend, i) => {
        if (!_vend.nombre.esValido || !_vend.sueldo.esValido) {
          this.listaVendedores.splice(i, 1);
        }
      }
    );

    const _listaVendedores = this.listaVendedores.length > 0 ?
        this.listaVendedores.map(
          (_vend: Usuario) => {
            if (_vend.nombre.esValido && _vend.sueldo.esValido) {
              return {
                'nombre': _vend.nombre.valor,
                'sueldo': _vend.sueldo.valor
              }
            }
          }) : [];

    return {
      'hayDescuentos': this.textoBooleano(this.hayDescuentos, true),
      'cambio': this.cambio.valor || 0,
      'listaDescuentos': _listaDescuentos,
      'listaVendedores': _listaVendedores,
      'fechaInicio': this.fechaInicio.valor || null,
      'fechaFin': this.fechaFin.valor || null,
      'iniciadaPor': this.iniciadaPor,
      'cerradaPor': this.cerradaPor,
      'montoEfectivo': this.montoEfectivo || 0,
      'montoTarjeta': this.montoTarjeta || 0
    };
  }
}
