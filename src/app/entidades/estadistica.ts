import { Fecha } from "./tipos/fecha";
import { Jornada } from "./jornada";

export class Estadistica {
  id: number;
  montoEfectivo: number;
  montoTarjeta: number;
  detalles: Jornada[];
  // FECHAS
  fecha: Fecha;
  // VARIABLES NO ENVIADAS A BE
  $key: any;

  constructor(_obj: any) {
    if (!_obj) {
      _obj = {}
    }

    this.id = _obj.id || 0;
    this.fecha = new Fecha(_obj.fecha);
    this.montoEfectivo = _obj.montoEfectivo || 0;
    this.montoTarjeta = _obj.montoTarjeta || 0;
    this.detalles =
      _obj.detalles && _obj.detalles.length > 0 ?
        _obj.detalles.map(
          (d: any) => new Jornada(d)
        ) : [];
    // VARIABLES NO ENVIADAS A BE
    this.$key = _obj.$key || null;
  }

  get fechaCorta(): string {
    const _fecha = this.fecha.valor.split('-');
    return `${_fecha[0]}-${_fecha[1]}`
  }

  toJS() {
    return {
      'fecha': this.fecha.valor,
      'montoEfectivo': this.montoEfectivo || 0,
      'montoTarjeta': this.montoTarjeta || 0,
    }
  }
}
