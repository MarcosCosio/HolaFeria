import { Numero } from "./tipos/numero";

export class MedioPago {
  descripcion: String;
  pagoParcial: Numero;
  interes: number;
  pagoFinal: number;

  constructor(_obj: any) {
    this.descripcion = _obj.descripcion;
    this.pagoParcial = new Numero(_obj.pagoParcial || 0);
    this.interes = _obj.interes;
    this.pagoFinal = _obj.pagoFinal;
  }

  get calcularPago() {
    const _pago = Number(this.pagoParcial.valor);
    return !isNaN(_pago) && _pago > 0 ?
      (_pago * (1 + (this.interes / 100))).toFixed(1) : 0;
  }

  toJS() {
    return {
      'descripcion': this.descripcion,
      'pagoParcial': this.pagoParcial.valor,
      'interes': this.interes,
      'pagoFinal': this.calcularPago
    };
  }
}
