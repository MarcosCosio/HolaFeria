export default {
  habilitarMocks: false,
  compararTexto: (_A: any, _B: any) => {
    return Boolean(_A) && Boolean(_B) && _A.toLowerCase() === _B.toLowerCase()
  }
};
