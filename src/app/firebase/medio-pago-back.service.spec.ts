import { TestBed } from '@angular/core/testing';

import { MedioPagoBackService } from './medio-pago-back.service';

describe('MedioPagoBackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MedioPagoBackService = TestBed.get(MedioPagoBackService);
    expect(service).toBeTruthy();
  });
});
