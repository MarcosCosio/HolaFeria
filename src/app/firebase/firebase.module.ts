import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// IMPORTAR SERVICIOS DE BACK END
import { JornadaBackService } from './jornada-back.service';
import { MedioPagoBackService } from './medio-pago-back.service';
import { VentaBackService } from './venta-back.service';
import { ArticuloBackService } from './articulo-back.service';
import { ArticuloElimBackService } from './articulo-elim-back.service';
import { TipoArticuloBackService } from './tipo-articulo-back.service';
import { EstadisticaBackService } from './estadistica-back.service';
import { UserBackService } from './user-back.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    MedioPagoBackService,
    JornadaBackService,
    VentaBackService,
    ArticuloBackService,
    ArticuloElimBackService,
    TipoArticuloBackService,
    EstadisticaBackService,
    UserBackService
  ]
})
export class FirebaseModule { }
