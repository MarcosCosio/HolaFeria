import { TestBed } from '@angular/core/testing';

import { PreciosModBackService } from './precios-mod-back.service';

describe('PreciosModBackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PreciosModBackService = TestBed.get(PreciosModBackService);
    expect(service).toBeTruthy();
  });
});
