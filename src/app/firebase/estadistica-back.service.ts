import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
// IMPORTAR ENTIDADES
import { Estadistica } from '../entidades/estadistica';
import { Venta } from '../entidades/venta';
import { Jornada } from '../entidades/jornada';
import { Articulo } from '../entidades/articulo';
// IMPORTAR MOMENT PARA COMPARAR FECHAS
import * as moment from 'moment';
// IMPORTAR NOMBRES DE BD PARA LAS BUSQUEDAS
import BD from '../configuraciones/basesDatos';

@Injectable({
  providedIn: 'root'
})
export class EstadisticaBackService {
  private lista: AngularFireList<any>;

  constructor(
    private firebase: AngularFireDatabase
  ) {
    this.lista = this.firebase.list(BD.ESTADISTICAS)
  }

  buscar_estadisticas() {
    return this.lista.snapshotChanges();
  }

  obtener_ultima_estadistica(): Promise<any> {
    const _promesa = new Promise(
      (resolve, reject) => {
        this.firebase.database.ref(BD.ESTADISTICAS)
          .limitToLast(1)
          .on('value',
            _resultado => {
              if (_resultado && _resultado.val()) {
                _resultado.forEach(
                  _item => {
                    const _estadistica = new Estadistica(_item.val());
                    _estadistica.$key = _item.key;
                    resolve(_estadistica)
                  }
                )
              } else {
                reject(false)
              }
            }
          )
      }
    )

    return _promesa;
  }

  obtener_ultimas_jornadas(): Promise<any> {
    const _promesa = new Promise(
      (resolve, reject) => {
        this.firebase.database.ref(BD.JORNADAS)
          .limitToLast(4)
          .on('value',
            _resultado => {
              if (_resultado && _resultado.val()) {
                let _listaJor = [];

                _resultado.forEach(
                  data => {
                    let _jornada = new Jornada(data.val());
                    _jornada.$key = data.key;
                    _jornada.id = _listaJor.length;
                    _listaJor.push(_jornada);
                  }
                )
                resolve(_listaJor);
              } else {
                reject(false);
              }
            }
          )
      }
    )

    return _promesa;
  }

  obtener_lista_jornadas(fechaEstad: string): Promise<any> {
    const _promesa = new Promise(
      (resolve, reject) => {
        let _resultado = [];

        this.firebase.database.ref(BD.JORNADAS)
          .orderByChild('fechaInicio')
          .startAt(fechaEstad)
          .on('value',
            _listaJor => {
              if (_listaJor && _listaJor.val()) {
                _listaJor.forEach(
                  data => {
                    const _jornada = new Jornada(data.val());
                    const _esMismoMes = moment(_jornada.fechaInicio.valor).isSameOrBefore(fechaEstad, 'month');

                    if(_esMismoMes) {
                      _jornada.$key = data.key;
                      _jornada.id = _resultado.length;
                      _resultado.push(_jornada);
                    }
                  }
                )

                if (_resultado.length > 0) {
                  _resultado =
                    _resultado.reverse().map(
                      (_item, i) => {
                        _item.id = i;
                        return _item;
                      }
                    )
                  }

                resolve(_resultado.length > 0 ? _resultado : false)
              } else {
                reject(false);
              }
            }
          );
      }
    )
    return _promesa;
  }

  obtener_lista_ventas(fechaJornada: string): Promise<any> {
    const _promesa = new Promise(
      (resolve, reject) => {
        let _resultado: Venta[] = [];

        this.firebase.database.ref(BD.VENTAS)
          .orderByChild('fechaVenta')
          .startAt(fechaJornada)
          .on('value',
            _listaVentas => {
              if (_listaVentas && _listaVentas.val()) {
                _listaVentas.forEach(
                  data => {
                    const _venta = new Venta(data.val());
                    _venta.$key = data.key;
                    _venta.id = _resultado.length;
                    _resultado.push(_venta);
                  }
                )

                resolve(_resultado.length > 0 ? _resultado : false);
              } else {
                reject(false);
              }
            }
          );
      }
    )
    return _promesa;
  }

  obtener_lista_articulos_vendidos(): Promise<any> {
    const _promesa = new Promise(
      (resolve, reject) => {
        let _resultado: Articulo[] = [];
        let _fechaHoy: any = moment(new Date());

        _fechaHoy.subtract(4, 'months')
        _fechaHoy = _fechaHoy.format('YYYY-MM-DD');

        this.firebase.database.ref(BD.VENTAS)
          .orderByChild('fechaVenta')
          .startAt(_fechaHoy)
          .on('value',
            _listaVentas => {
              if(_listaVentas && _listaVentas.val()) {
                _listaVentas.forEach(
                  data => {
                    const _venta = new Venta(data.val());

                    _venta.articulos.forEach(
                      _articulo => {
                        _articulo.fechaVenta = _venta.fechaVenta.valor;
                        _articulo.vendedor = _venta.vendedor;
                        _resultado.push(_articulo);
                      }
                    );
                  }
                );

                resolve(_resultado.length > 0 ? _resultado.reverse() : false)
              } else {
                reject(false);
              }
            }
          );
      }
    )

    return _promesa;
  }

  crear_estadistica(estadistica: Estadistica) {
    this.lista.push(estadistica.toJS());
  }

  mod_estadistica(estadistica: Estadistica) {
    this.lista.update(estadistica.$key, estadistica.toJS());
  }
}
