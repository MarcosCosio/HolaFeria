import { TestBed, inject } from '@angular/core/testing';

import { UserBackService } from './user-back.service';

describe('UserBackService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserBackService]
    });
  });

  it('should be created', inject([UserBackService], (service: UserBackService) => {
    expect(service).toBeTruthy();
  }));
});
