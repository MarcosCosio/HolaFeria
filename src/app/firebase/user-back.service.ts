import { Injectable } from '@angular/core';
// IMPORTAR ENTIDADES
import { Usuario } from '../entidades/usuario';
// IMPORTAR NOMBRES DE BD PARA LAS BUSQUEDAS
import BD from '../configuraciones/basesDatos';
// IMPORTAR CLASE DE FIREBASE PARA EXTENDER FUNCIONALIDADES COMUNES DE CONEXION A BD
import { FirebaseService } from '../entidades/interfaces/firebaseService';
import { AngularFireDatabase } from "angularfire2/database";

@Injectable()
export class UserBackService extends FirebaseService{
  constructor(
    private firebaseService: AngularFireDatabase
  ) {
    super(BD.USUARIOS, firebaseService)
  }

  obtener_usuario(pass: string, soloAdmin: boolean = false) {
    let _resultado: Usuario;

    this.firebase.database.ref(this.nombreBd)
      .orderByChild('pass')
      .equalTo(pass)
      .on('value',
        _user => {
          if (_user) {
            _user.forEach(
              data => {
                const usuario = new Usuario(data.val());

                if (usuario.estado.valor &&
                    ((soloAdmin && !usuario.esVendedor()) || !soloAdmin)) {
                  _resultado = usuario
                  _resultado.$key = data.key;
                }
              }
            )
          }
        }
      );
    return _resultado || false;
  }
}
