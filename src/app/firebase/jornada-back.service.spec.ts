import { TestBed } from '@angular/core/testing';

import { JornadaBackService } from './jornada-back.service';

describe('JornadaBackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JornadaBackService = TestBed.get(JornadaBackService);
    expect(service).toBeTruthy();
  });
});
