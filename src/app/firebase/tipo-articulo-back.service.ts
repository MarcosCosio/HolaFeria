import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
// IMPORTAR MOCKS
import Mocks from '../entidades/mocks/tipoArticulos.mock';
// IMPORTAR NOMBRES DE BD PARA LAS BUSQUEDAS
import BD from '../configuraciones/basesDatos';

@Injectable({
  providedIn: 'root'
})
export class TipoArticuloBackService {
  private lista: AngularFireList<any>;

  constructor(
    private firebase: AngularFireDatabase
  ) {
    this.lista = this.firebase.list(BD.TIPOS_ARTICULOS);
  }

  buscar_tipoArticulo() {
    return this.lista.snapshotChanges();
  }

  init_tipoArticulo() {
    this.lista.push(Mocks);
  }

  editar_tipoArticulo($key, tipo_articulo_editado) {
    this.lista.set($key, tipo_articulo_editado);
  }

  eliminar_Bd() {
    this.lista.remove();
  }
}
