import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
// IMPORTAR ENTIDADES
import { Jornada } from '../entidades/jornada';
// IMPORTAR NOMBRES DE BD PARA LAS BUSQUEDAS
import BD from '../configuraciones/basesDatos';

@Injectable({
  providedIn: 'root'
})
export class JornadaBackService {
  private _backService: AngularFireList<any>;

  constructor(
    private firebase: AngularFireDatabase
  ) {
    this._backService = this.firebase.list(BD.JORNADAS);
  }

  crear_jornada(_jornada: Jornada) {
    this._backService.push(_jornada.toJS());
  }

  mod_jornada(_jornada: Jornada) {
    this._backService.update(
      _jornada.$key, _jornada.toJS()
    );
  }

  traer_jornada(): Promise<any> {
    const _promesa = new Promise(
      (resolve, reject) => {
        this.firebase.database.ref(BD.JORNADAS)
          .limitToLast(1).on('value',
            _resultado => {
              if (_resultado && _resultado.val()) {
                _resultado.forEach(
                  data => {
                    const _jor = new Jornada(data.val());
                    _jor.$key = data.key;
                    resolve(_jor);
                  }
                )
              } else {
                reject(false);
              }
            }
          )
      }
    )

    return _promesa;
  }
}
