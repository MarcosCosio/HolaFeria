import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import Mocks from '../entidades/mocks/medioPago.mock';
// IMPORTAR NOMBRES DE BD PARA LAS BUSQUEDAS
import BD from '../configuraciones/basesDatos';

@Injectable({
  providedIn: 'root'
})
export class MedioPagoBackService {
  private _backService: AngularFireList<any>;

  constructor(
    private firebase: AngularFireDatabase
  ) {
    this._backService = this.firebase.list(BD.MEDIOS_PAGO);
  }

  buscar_medioPago() {
    return this._backService.snapshotChanges();
  }

  init_medioPago() {
    this._backService.push(Mocks);
  }

  eliminar_Bd() {
    this._backService.remove();
  }
}
