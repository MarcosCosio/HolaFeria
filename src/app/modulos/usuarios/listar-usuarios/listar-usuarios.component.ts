import { Component, HostBinding } from '@angular/core';
// IMPORTAR SERVICIOS
import { NavegadorService } from '../../../servicios/navegador.service';
import { UserService } from '../../../servicios/usuario.service';
// IMPORTAR ENTIDADES
import { Usuario } from '../../../entidades/usuario';

@Component({
  selector: 'listar-usuarios',
  templateUrl: './listar-usuarios.component.html',
  styleUrls: ['./listar-usuarios.component.scss']
})
export class ListarUsuariosComponent {
  @HostBinding('class') clases = 'cuerpo';
  usuario: Usuario;
  listaUsuarios: Usuario[];

  constructor(
    private _userService: UserService,
    private _navService: NavegadorService) {
    this.listaUsuarios = _userService.listaUsuarios;
  }

  verDetalles(i: any) {
    this._userService.verDatosUsuario(i);
    this._navService.procesarRuta(['usuarios', 'ver', i]);
  }
}
