import { Component, HostBinding } from '@angular/core';
import { MedioPagoService } from '../../servicios/medioPago.service';

@Component({
  selector: 'usuarios',
  templateUrl: './usuarios.component.html',
})
export class UsuariosComponent {
  @HostBinding('class') clases = 'col-md-12';

  constructor(
    private _medioPagoService: MedioPagoService,
  ) {
    // this._medioPagoService.eliminarBd();
    // this._medioPagoService.cargarBd();
  }
}
