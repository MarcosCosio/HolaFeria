import { Component, HostBinding } from '@angular/core';
// IMPORTAR SERVICIOS
import { UserService } from '../../../servicios/usuario.service';
// IMPORTAR ENTIDADES
import { Usuario } from '../../../entidades/usuario';

@Component({
  selector: 'crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.scss']
})
export class CrearUsuarioComponent {
  @HostBinding('class') clases = 'cuerpo';
  usuarioCreado: Usuario;

  constructor(private _userService: UserService) {
    this.usuarioCreado = this._userService.usuarioMod;
  }
}
