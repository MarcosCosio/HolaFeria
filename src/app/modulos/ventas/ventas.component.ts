import { Component, HostBinding, OnInit } from '@angular/core';
import { JornadaService } from '../../servicios/jornada.service';
import { EstadisticaService } from '../../servicios/estadistica.service';
import { MedioPagoService } from '../../servicios/medioPago.service';
import { UserService } from '../../servicios/usuario.service';
import { TipoArticuloService } from '../../servicios/tiposArt.service';
import { StockService } from '../../servicios/stock.service';

@Component({
  selector: 'ventas',
  templateUrl: './ventas.component.html'
})
export class VentasComponent implements OnInit {
  @HostBinding('class') clases = 'col-md-12';

  constructor(
    private _jornadaService: JornadaService,
    private _estadisticasService: EstadisticaService,
    private _usuarioService: UserService,
    private _tipoArticuloService: TipoArticuloService,
    private _stockService: StockService,
  ) { }

  ngOnInit() {
    // METODOS PARA INICIAR LA BD DESDE CERO
    // this._estadisticasService.crearNuevaEstadistica();
    // this._usuarioService.cargarBd();
    // this._tipoArticuloService.cargarBd();
    //--------------------------------------------------------
    this._jornadaService.marcarRutaVentas();
    this._estadisticasService.traerUltimasJornadas();
  }
}
