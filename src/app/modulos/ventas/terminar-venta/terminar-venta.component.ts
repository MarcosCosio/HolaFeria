import { Component, HostBinding } from '@angular/core';
// IMPORTAR SERVICIOS
import { VentaService } from '../../../servicios/venta.service';
// IMPORTAR ENTIDADES
import { Venta } from '../../../entidades/venta';

@Component({
  selector: 'app-terminar-venta',
  templateUrl: './terminar-venta.component.html',
  styleUrls: ['./terminar-venta.component.scss']
})
export class TerminarVentaComponent {
  @HostBinding('class') clases = 'cuerpo';
  nuevaVenta: Venta;

  constructor(
    private _ventaService: VentaService
  ) {
    this.nuevaVenta = this._ventaService.ventaEnCurso;
  }
}
