import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// MODULO => RUTEO DE ANGULAR
import { RoutingModule } from '../app-routing.module';
// IMPORTAR COMPONENTES
import { NavegadorComponent } from './encabezado/navegador/navegador.component';
import { BotoneraComponent } from './encabezado/botonera/botonera.component';

@NgModule({
  declarations: [
    NavegadorComponent,
    BotoneraComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RoutingModule
  ],
  exports: [
    NavegadorComponent,
    BotoneraComponent
  ]
})
export class EncabezadoModule { }
