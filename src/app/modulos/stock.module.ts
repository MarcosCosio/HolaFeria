import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// MODULO => RUTEO DE ANGULAR
import { RoutingModule } from '../app-routing.module';
// MODULO => MODAL DE BOOTSTRAP
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// IMPORTAR COMPONENTES
import { StockComponent } from './stock/stock.component';
import { ListarStockComponent } from './stock/listar-stock/listar-stock.component';
import { AgregarStockComponent } from './stock/agregar-stock/agregar-stock.component';
import { VerPreciosModComponent } from './stock/ver-precios-mod/ver-precios-mod.component';
import { VerArticulosDelComponent } from './stock/ver-articulos-del/ver-articulos-del.component';

@NgModule({
  declarations: [
    StockComponent,
    ListarStockComponent,
    AgregarStockComponent,
    VerPreciosModComponent,
    VerArticulosDelComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    RoutingModule
  ],
  exports: [
    StockComponent,
    ListarStockComponent,
    AgregarStockComponent,
    VerPreciosModComponent,
    VerArticulosDelComponent
  ]
})
export class StockModule { }
