import { Component, HostBinding } from '@angular/core';
import { EstadisticaService } from '../../servicios/estadistica.service';

@Component({
  selector: 'estadisticas',
  templateUrl: './estadisticas.component.html',
  styleUrls: ['./estadisticas.component.scss']
})
export class EstadisticasComponent {
  @HostBinding('class') classes = 'col-md-12';

  constructor(
    private _estadisticaService: EstadisticaService
  ) {
    this._estadisticaService.traerUltimasJornadas();
   }
}
