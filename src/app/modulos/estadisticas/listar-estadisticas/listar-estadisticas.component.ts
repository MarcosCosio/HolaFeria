import { Component, HostBinding } from '@angular/core';
// IMPORTAR SERVICIOS
import { EstadisticaService } from '../../../servicios/estadistica.service';
// IMPORTAR ENTIDADES
import { Estadistica } from '../../../entidades/estadistica';
import { Jornada } from '../../../entidades/jornada';

@Component({
  selector: 'listar-estadisticas',
  templateUrl: './listar-estadisticas.component.html',
  styleUrls: ['./listar-estadisticas.component.scss']
})
export class ListarEstadisticasComponent {
  @HostBinding('class') clases = 'cuerpo';
  ultimasJornadas: Jornada[];
  listaMeses: Estadistica[] = [];

  constructor(
    private _estadisticaService: EstadisticaService
  ) {
    this.ultimasJornadas = this._estadisticaService.ultimasJornadas;
    this.listaMeses = this._estadisticaService.listaMeses;
  }

  verJornada(i: any) {
    this._estadisticaService.verDetallesJornada(i, true);
  }

  verMes(i: any) {
    this._estadisticaService.verDetallesMes(i);
  }
}
