import { Component, HostBinding } from '@angular/core';
// IMPORTAR SERVICIOS
import { EstadisticaService } from '../../../servicios/estadistica.service';
// IMPORTAR ENTIDADES
import { Venta } from '../../../entidades/venta';

@Component({
  selector: 'ver-venta',
  templateUrl: './ver-venta.component.html',
  styleUrls: ['./ver-venta.component.scss']
})
export class VerVentaComponent {
  @HostBinding('class') clases = 'cuerpo';
  venta: Venta;
  titulo = 'Detalles de la venta';

  constructor(
    private _estadisticasService: EstadisticaService
  ) {
    this.venta = this._estadisticasService.detallesVenta;
  }
}
