import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerJornadaComponent } from './ver-jornada.component';

describe('VerJornadaComponent', () => {
  let component: VerJornadaComponent;
  let fixture: ComponentFixture<VerJornadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerJornadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerJornadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
