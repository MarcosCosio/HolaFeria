import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerEstadisticaComponent } from './ver-estadistica.component';

describe('VerEstadisticaComponent', () => {
  let component: VerEstadisticaComponent;
  let fixture: ComponentFixture<VerEstadisticaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerEstadisticaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerEstadisticaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
