import { Component, HostBinding, OnInit } from '@angular/core';
// IMPORTAR SERVICIOS
import { StockService } from '../../../servicios/stock.service';
import { SeguridadService } from '../../../servicios/seguridad.service';
// IMPORTAR ENTIDADES
import { Articulo } from '../../../entidades/articulo';
import { PrecioMod } from '../../../entidades/precioMod';
// IMPORTAR UTILIDADES
import Mensajes from '../../../configuraciones/mensajes';

@Component({
  selector: 'listar-stock',
  templateUrl: './listar-stock.component.html',
  styleUrls: ['./listar-stock.component.scss']
})
export class ListarStockComponent implements OnInit {
  @HostBinding('class') clases = 'cuerpo';
  titulo: string;
  listaArticulos: Articulo[];
  listaTotales;
  hayArticulos: boolean;
  precioMod: PrecioMod;

  constructor(
    private _stockService: StockService,
    private _seguridadService: SeguridadService
  ) {
    this.listaArticulos = this._stockService.listaArticulos;
    this.listaTotales = this._stockService.listaTotalesPorArticulo;
    this.hayArticulos = this._stockService.hayArticulos;
    this.precioMod = this._stockService.precioMod;
    this.titulo = this.hayArticulos ?
      'Lista de artículos en stock' :
      'No hay artículos en stock por el momento. Para cargar su stock, haga click en el boton "Agregar artículo/s".'
  }

  ngOnInit() {
    this.listaArticulos = this._stockService.listaArticulos;
  }

  mostrarPrecioUi(art: Articulo) {
    this._stockService.mostrarPrecioUi(art);
  }

  confirmarCambiarPrecio() {
    this._seguridadService.navegar({
      esLlamadaValida: 'stock/precioModificadoValido',
      necesitaAcceso: true,
      soloAdmin: true,
      msjModal: Mensajes.cambiarPrecioArticulo,
      servicio: `stock/modificarPrecio`,
      rutaFinal: 'stock'
    })
  }

  cancelarCambiarPrecio() {
    this._stockService.cancelarCambiarPrecio();
  }

  eliminarDelStock(art: Articulo) {
    this._seguridadService.navegar({
      necesitaAcceso: true,
      soloAdmin: true,
      msjModal: Mensajes.eliminarArticulo,
      servicio: `stock/eliminarArticulo/${art.$key}`,
      rutaFinal: 'stock'
    });
  }
}
