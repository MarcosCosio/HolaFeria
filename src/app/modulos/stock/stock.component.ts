import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'stock',
  templateUrl: './stock.component.html'
})
export class StockComponent  {
  @HostBinding('class') clases = 'col-md-12';

  constructor() { }
}
