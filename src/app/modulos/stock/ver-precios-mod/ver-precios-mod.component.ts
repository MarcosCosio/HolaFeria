import { Component, HostBinding } from '@angular/core';
import { PrecioMod } from '../../../entidades/precioMod';
import { PreciosModService } from '../../../servicios/preciosMod.service';

@Component({
  selector: 'ver-precios-mod',
  templateUrl: './ver-precios-mod.component.html',
  styleUrls: ['./ver-precios-mod.component.scss']
})
export class VerPreciosModComponent  {
  @HostBinding('class') clases = 'cuerpo';
  listaPreciosMod: PrecioMod[];
  titulo: string;

  constructor(
    private _precioModService: PreciosModService
  ) {
    this.listaPreciosMod = this._precioModService.listaPreciosMod;
    this.titulo = this.listaPreciosMod.length > 0 ?
      'Listado de precios modificados' :
      'No hay precios modificados por el momento.'
  }
}
