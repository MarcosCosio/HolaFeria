import { Component, HostBinding } from '@angular/core';
import { ArticuloEliminado } from '../../../entidades/articuloElim';
import { ArticuloElimService } from '../../../servicios/articuloElim.service';

@Component({
  selector: 'app-ver-articulos-del',
  templateUrl: './ver-articulos-del.component.html',
  styleUrls: ['./ver-articulos-del.component.scss']
})
export class VerArticulosDelComponent {
  @HostBinding('class') clases = 'cuerpo';
  listaArticulosElim: ArticuloEliminado[];
  titulo: string;

  constructor(
    private _articuloElimService: ArticuloElimService
  ) {
    this.listaArticulosElim = this._articuloElimService.listaArticulosElim;
    this.titulo = this.listaArticulosElim.length > 0 ?
      'Listado de artículos eliminados' :
      'No hay artículos eliminados por el momento.'
  }
}
