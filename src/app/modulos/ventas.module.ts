import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// MODULO => RUTEO DE ANGULAR
import { RoutingModule } from '../app-routing.module';
// MODULO => MODAL DE BOOTSTRAP
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// IMPORTAR COMPONENTES
import { VentasComponent } from './ventas/ventas.component';
import { NuevaJornadaComponent } from './ventas/nueva-jornada/nueva-jornada.component';
import { NuevaVentaComponent } from './ventas/nueva-venta/nueva-venta.component';
import { TerminarVentaComponent } from './ventas/terminar-venta/terminar-venta.component';

@NgModule({
  declarations: [
    VentasComponent,
    NuevaJornadaComponent,
    NuevaVentaComponent,
    TerminarVentaComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    RoutingModule
  ],
  exports: [
    VentasComponent,
    NuevaJornadaComponent,
    NuevaVentaComponent,
    TerminarVentaComponent,
  ]
})
export class VentasModule { }
