import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// IMPORTAR GUARDAS
import { SeguridadGuard } from './seguridad.guard';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    SeguridadGuard
  ]
})
export class GuardasModule { }
